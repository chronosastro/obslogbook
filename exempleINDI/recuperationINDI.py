from flask import Flask,jsonify
from flask_pymongo import PyMongo
import random
import PyIndi
import sys
import logging
import time


class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
        self.logger = logging.getLogger('IndiClient')
        self.logger.info('creating an instance of IndiClient')
    def newDevice(self, d):
        self.logger.info("new device " + d.getDeviceName())
    def newProperty(self, p):
        self.logger.info("new property "+ p.getName() + " for device "+ p.getDeviceName())
    def removeProperty(self, p):
        self.logger.info("remove property "+ p.getName() + " for device "+ p.getDeviceName())
    def newBLOB(self, bp):
        self.logger.info("new BLOB "+ bp.name)
    def newSwitch(self, svp):
        self.logger.info ("new Switch "+ svp.name + " for device "+ svp.device)
    def newNumber(self, nvp):
        self.logger.info("new Number "+ nvp.name + " for device "+ nvp.device)
    def newText(self, tvp):
        self.logger.info("new Text "+ tvp.name + " for device "+ tvp.device)
    def newLight(self, lvp):
        self.logger.info("new Light "+ lvp.name + " for device "+ lvp.device)
    def newMessage(self, d, m):
        self.logger.info("new Message "+ d.messageQueue(m))
    def serverConnected(self):
        self.logger.info("Server connected ("+self.getHost()+":"+str(self.getPort())+")")
    def serverDisconnected(self, code):
        self.logger.info("Server disconnected (exit code = "+str(code)+","+str(self.getHost())+":"+str(self.getPort())+")")

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/test"
mongo = PyMongo(app)

@app.route('/insert/devices')
def insertDevices():
	# Create an instance of the IndiClient class and initialize its host/port members
	indiclient=IndiClient()
	indiclient.setServer("localhost",7624)

	# Connect to server
	print("Connecting and waiting 1 sec")
	if (not(indiclient.connectServer())):
     		print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
     		print("  indiserver indi_simulator_telescope indi_simulator_ccd")
     		sys.exit(1)
	time.sleep(1)

	# Print list of devices. The list is obtained from the wrapper function getDevices as indiclient is an instance
	# of PyIndi.BaseClient and the original C++ array is mapped to a Python List. Each device in this list is an
	# instance of PyIndi.BaseDevice, so we use getDeviceName to print its actual name.
	print("List of devices")
	dl=indiclient.getDevices()
	devices=[]
	for device in dl:
		devices.append(str(device.getDeviceName()))
	mongo.db.test.insert_one({"devices":devices})
	return "Document insert"




@app.route('/get/all')
def insertLocalisation():
	ret={}
	indiclient=IndiClient()
	indiclient.setServer("localhost",7624)
	# Connect to server
	print("Connecting and waiting 1 sec")
	if (not(indiclient.connectServer())):
		print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run")
		print("  indiserver indi_simulator_telescope indi_simulator_ccd")
		sys.exit(1)
	time.sleep(1)

	dl=indiclient.getDevices()
	for device in dl:
		#if the device is the telescope (interface = 5)
		if(str(device.getDriverInterface())=="5"):
			device_telescope=indiclient.getDevice(device.getDeviceName())
			ret['telescope']=device.getDeviceName()
		#if the device is the ccd (interface = 22)
		elif(str(device.getDriverInterface())=="22"):
			device_ccd=indiclient.getDevice(device.getDeviceName())
			ret['ccd']=device.getDeviceName()


	#Get all the information with telescope if it was found before
	if(device_telescope):
		# wait CONNECTION property be defined for telescope
		telescope_connect=device_telescope.getSwitch("CONNECTION")
		while not(telescope_connect):
			time.sleep(0.5)
			telescope_connect=device_telescope.getSwitch("CONNECTION")

		# if the telescope device is not connected, we do connect it
		if not(device_telescope.isConnected()):
			# Property vectors are mapped to iterable Python objects
			# Hence we can access each element of the vector using Python indexing
			# each element of the "CONNECTION" vector is a ISwitch
			telescope_connect[0].s=PyIndi.ISS_ON  # the "CONNECT" switch
			telescope_connect[1].s=PyIndi.ISS_OFF # the "DISCONNECT" switch
			indiclient.sendNewSwitch(telescope_connect) # send this new value to the device

		#get telescope informations:
		telescopeInformation = getTelescopeInfos(device_telescope)
		print("-----TELESCOPE INFORMATIONS-----")
		for info in telescopeInformation:
			print(str(info.name)+": "+str(info.value))

		#Get the localisation of the telescope
		localisation = getLocalisation(device_telescope)
		print(str(localisation))
		print("-----LOCALISATION-----")
		print("Latitude: ",str(localisation[0].value))
		ret['latitude']=str(localisation[0].value)

		print("Longitude: ",str(localisation[1].value))
		ret['longitude']=str(localisation[1].value)

		print("Elevation: ",str(localisation[2].value))
		ret['elevation']=str(localisation[2].value)

	else:
		print("No telescope found")

	#Get all the information with ccd if it was found before
	if(device_ccd):
		ccd_connect=device_ccd.getSwitch("CONNECTION")
		while not(ccd_connect):
			time.sleep(0.5)
			ccd_connect=device_ccd.getSwitch("CONNECTION")
		if not(device_ccd.isConnected()):
			ccd_connect[0].s=PyIndi.ISS_ON
			ccd_connect[1].s=PyIndi.ISS_OFF
			indiclient.sendNewSwitch(ccd_connect)

		ccdInformation = getCCDInfos(device_ccd)
		print("-----CCD INFORMATIONS-----")
		for info in ccdInformation:
			print(str(info.name)+": "+str(info.value))
			ret[info.name]=str(info.value)


		print("-----CCD TEMPERATURE-----")
		ccdTemperature = device_ccd.getNumber("CCD_TEMPERATURE")
		print(str(ccdTemperature[0].value))
		ret["temperature"]=str(ccdTemperature[0].value)


		print("-----CCD FRAME-----")
		ccdFrame = device_ccd.getNumber("CCD_FRAME")
		for info in ccdFrame:
			print(str(info.name)+": "+str(info.value))
			ret[info.name]=str(info.value)


		print("-----CCD BINNING-----")
		ccdFrame = device_ccd.getNumber("CCD_BINNING")
		for info in ccdFrame:
			print(str(info.name)+": "+str(info.value))
			ret[info.name]=str(info.value)

		print("-----CCD EXPOSURE-----")
		ccdExposure = device_ccd.getNumber("CCD_EXPOSURE")
		print(str(ccdExposure[0].value))
		ret["exposure"]=str(ccdExposure[0].value)


	else:
		print("no ccd found")


	indiclient.disconnectServer()
	return jsonify(ret)


def getLocalisation(telescope):
	return telescope.getNumber("GEOGRAPHIC_COORD")

def getTelescopeInfos(telescope):
	return telescope.getNumber("TELESCOPE_INFO")

def getCCDInfos(ccd):
	print(str(ccd.getDeviceName()))
	return ccd.getNumber("CCD_INFO")

if __name__ == '__main__':
	app.run(debug=True)

import * as React from 'react';
import axios from 'axios';
import Charts from "./charts"
import 'antd/dist/antd.css';
import './css/search.css'; 
import {Container,Row,Button,ButtonGroup,Dropdown} from'react-bootstrap';
import DisplayObsResult from './searchComponents/displayObsResult';
import DisplayTable from './searchComponents/displayTable';
import { Col } from 'antd';
import { faSearch, faSyncAlt} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ImportPopup from "./popups/importPopup";

/**
This class is the one displayed for the search tab of the application, we use each sub component such as observationResult.jsx and charts.jsx here
*/
class search extends React.Component {
    constructor(props) {
		super(props);
		this.urlAPI=props.urlAPI;
		this.tableDisp=React.createRef();
		this.charts=React.createRef();  
        this.state = {
            listObs:[],
			currentObsDisplay:'',
			searchInput:'',
			filterValue:'object_name',
			typeValue:'',
			cooldown:false,
			timeout:"",
			popupvisible: false
		}
		
		
	}
	/** Set data table on load of the component */
	componentDidMount() {
		this.getObs();
	  }
	/**
	Change the currentObsDisplay state with the obs provided, this happen when an user click on Display button inside antd Table
	When the state is changed the observationResult props changed too and the observation informations are displayed.
	*/
	displayObs = (obs) => {
        this.setState({
            ...this.state,
            currentObsDisplay: obs
        });
    };

	/**
	reset the currentObsDisplay, this happen when an user click on Hide button
	*/
	hideObs = async () => {
        let state = this.state;
        await this.setState({
            ...state,
            currentObsDisplay: ''
        });
    };
	
	/** This function add a timeout to request for bdd */
	getObsWithTimeout=async()=>{
		if(!this.state.cooldown){
			this.setState({cooldown:true});
			this.setState({timeout:setTimeout(()=>{ this.setState({cooldown:false}); this.getObs()}, 800)});
		}
		else{
			clearTimeout(this.state.timeout);
			this.setState({timeout:setTimeout(()=>{ this.setState({cooldown:false}); this.getObs()}, 800)});
			return;
		}
	}
	/**
	This function get observation depending on filter selected by the user and input's value
	We defined the request url depending of the filter type and then get observations from the API
	*/
    getObs= async()=> {
		
		var urlApi = '';
		var title=window["translations"]().title;
		if(this.state.searchInput===""){
			this.state.typeValue==="" ? urlApi = this.urlAPI+'/get/obs/' : 
											urlApi = this.urlAPI+'/get/obsType/'+this.state.typeValue;
		}
		else{
			switch (this.state.filterValue) {
				case 'date' :
					var dateFormated=this.state.searchInput.slice();
					dateFormated=dateFormated.split('/').join('-');
					urlApi = this.urlAPI+'/get/obsDate/'+dateFormated;
					break;
				case 'object_name':
					urlApi = this.urlAPI+'/get/obsObjectName/'+this.state.searchInput;
					break;
				case 'session_id':
					urlApi = this.urlAPI+'/get/obsSessionId/'+this.state.searchInput;
					break;
				default :
					break;
			}
			if(this.state.typeValue!=="" )
				urlApi+="/"+this.state.typeValue;
		};
		let currentComponent = this;
		let tabled=this.tableDisp.current;
        axios.get(urlApi)
            .then(async function (response) {
				response.data.length===0? 
				await currentComponent.setState({
                    listObs: response.data,
					errorMsg: title.errorMatch
				})
				:await currentComponent.setState({
                    listObs: response.data,
					errorMsg: ''
				});
				tabled.setState({listObs:response.data})
            })
            .catch(async function (error) {
							
							await currentComponent.setState({
									listObs:[],
									errorMsg: title.errorInput
							})
							tabled.setState({listObs:[]})
            });				
	}

	/**
	 * refresh : refresh the data on the search page
	 */
	refresh = async () => {
		this.getObs();
		if ( this.charts.current != null ) {
			this.charts.current.getTotalObs();
			this.charts.current.getTheMostPopular();
			this.charts.current.getTotalExposure();
		}
	}
	/**
	 * Modify an observation
	 */
	modify= (data)=>{
		var finalData={};
		Object.keys(data).forEach(key=>{
			if(Object.keys(data[key]).length!==0 && !(typeof data[key]=== 'string')){
				Object.keys(data[key]).forEach(k=>{
					finalData[k]=data[key][k];
				})
			}
			else{
				if(typeof data[key]=== 'string'){
					finalData[key]=data[key];
				}
			}
		});
		window["navBar"].current.obsearch();
		finalData["saved"]=true;
		this.props.inputList.current.setState(finalData);
	}

	/**
	 * export : create a json file to download with all data of the database
	 */
	export = () => {
		var fullJSON = JSON.parse(JSON.stringify(this.state.listObs));
		for (var i=0 ; i < fullJSON.length ; i++ ) {
			delete fullJSON[i]["_id"];
		}
		var JSONvalue=JSON.stringify(fullJSON,null,'\t');

		// Create a input to copy the string array inside it
		var textToCopy = document.createElement("textarea");

		// Add it to the document
		document.body.appendChild(textToCopy);

		// Set its ID
		textToCopy.setAttribute("id", "textToDownload_id");

		// Output the array into it
		document.getElementById("textToDownload_id").value=JSONvalue


		const element = document.createElement("a");
		const file = new Blob([document.getElementById('textToDownload_id').value], {type: 'application/json'});
		element.href = URL.createObjectURL(file);
		//Name of the downloaded file
		element.download = "obslogbook_database.json";
		document.body.appendChild(element); // Required for this to work in FireFox
		element.click();
		document.body.removeChild(textToCopy);
		document.body.removeChild(element);
	}

	/**
	 * openPopup : open the import popup
	 */
	openPopup = () => {
		if ( this.state.popupvisible === false ) {
			this.setState({
				popupvisible: true
			});
		}
	}
	/**
	 * closePopup : close the import popup
	 */
	closePopup = () => {
		if ( this.state.popupvisible === true ) {
			this.setState({
				popupvisible: false
			});
		}
	}

	/**
	First we defined antd Table columns (see:https://ant.design/components/table/)
	Then depending on the state.filterValue value we render a specific form.
	For exemple if "type" is selected we display a Select form. Fo reach filter except Type, there is also a select for the observation's type
	*/
    render() {	
		
		var button=window["translations"]().button;
		var title=window["translations"]().title;
		return (
			<Container className="w-100 mw-100">
				<Row className="d-flex justify-content-center" >
					<div className={"searchObs w-75"}>
						<h5 className={"text-center mb-1 searchTitle"}><br/>{title.searchObservations}</h5>
							<label className="text-center ml-4 mt-3 w-100 ">
								<Row className="w-100" >
									<Col className="d-flex inputCol">
										<input type="text" className="w-100"  
										placeholder={title.searchFilter} 
										name="searchInput" 
										disabled={this.state.filterValue===""?true:false}
										readOnly={this.state.filterValue===""?true:false}
										value={this.state.searchInput}
										onChange={({target})=>{this.setState({searchInput:target.value});this.getObsWithTimeout();}}/>
									</Col>
									<Col className="d-flex searchCol" >
										<Button id={"searchButton"} type="button" onClick={this.refresh} className={"btn purple-bg ml-2 w-100 searchButton" }><FontAwesomeIcon icon={faSearch}/></Button>
										<Button id={"exportButton"} type="button" onClick={()=>{this.export()}} className={"btn purple-bg ml-2 w-100 searchButton" }>{button.export}</Button>
										<Button id={"importButton"} type="button" onClick={()=>{this.openPopup()}} className={"btn purple-bg ml-2 w-100 searchButton" }>{button.import}</Button>
									</Col>
								</Row>
							</label>
							<span className="ml-2" id="errorMsg">{this.state.errorMsg}</span>
							<hr/>
							
							<div className="w-100 mt-1 d-flex justify-content-center">
								<Row className={"w-100"} lg={24}>
								<Col className={"colFiltre"} lg={6}>
								<div className="ml-3 mt-1 filter">
									{button.filter}
								</div>
							
								
								<Dropdown className="ml-2 w-100">
										<Dropdown.Toggle id="dropdown-basic" className="purple-bg w-100">
											{button[this.state.filterValue]}
										</Dropdown.Toggle>

										<Dropdown.Menu onClick={({target})=>{this.setState({filterValue:target.id!==""?target.id:"object_name"});this.getObs();}}>
											<Dropdown.Item id="object_name">{button.object_name}</Dropdown.Item>
											<Dropdown.Item id="date">{button.date}</Dropdown.Item>
											<Dropdown.Item id="session_id">{button.session_id}</Dropdown.Item>
										</Dropdown.Menu>
								</Dropdown>
								</Col>
								<Col className={"colFiltre"} lg={6}>
								<div className="ml-3 mb-4 filter w-100">
									
									{button.type} {"  "}
									<ButtonGroup id="btn-group-type" className="mr-2" aria-label="First group" 
									onClick={async ({target})=>{
										await this.setState({typeValue:target.value});
										this.getObs();
										document.getElementById("btn-group-type").childNodes.forEach(x=>x.className="btn btn-secondary btnType w-100")
										target.className="btn purple-bg btnType  w-100";
										
									}} 
									value={this.state.typeValue}>
										<Button id="all"  value="" className="purple-bg btnType w-100">{button.all}</Button>{' '}
										<Button id="visual" className="btnType  w-100" value="visual"variant="secondary" >{button.visual}</Button>{' '}
										<Button id="astrophoto" className="btnType  w-100" value="astro" variant="secondary">{button.astrophoto}</Button>{' '}
										<Button id="spectrograph" className="btnType  w-100" value="spectro" variant="secondary">{button.spectrograph}</Button>
									</ButtonGroup>
								</div>
								</Col>
								<Col lg={6}></Col>
								<Col lg={6} className="colRefresh">
									<div className="mt-4 filter" id="refresh">
									<Button id="searchButton" onClick={this.refresh} className="purple-bg mr-2 refreshButton"><FontAwesomeIcon icon={faSyncAlt}/></Button>
									</div>
								</Col>
								</Row>
							</div>
						
						</div>
					</Row>
					<Row className="d-flex justify-content-center">
						<div className={"searchObs w-75 ml-5 mr-5"}>

							<DisplayTable id={"selectFilter"} ref={this.tableDisp} displayObs={this.displayObs} modify={this.modify}/>

						</div>
					</Row>
					<Row className="chart" >
							<Charts ref={this.charts} urlAPI={this.urlAPI}/>
					</Row>	
				<DisplayObsResult observation={this.state.currentObsDisplay} hide={this.hideObs}/>
				<ImportPopup
				    visible={this.state.popupvisible}
					closePopup={this.closePopup}
					urlAPI={this.urlAPI}
                />
		</Container>
		);
		
				
    }
}
export default search;

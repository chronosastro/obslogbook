import * as React from 'react';
import axios from 'axios';
import Charts from "./charts";


//SessionTracker is the component on the left which shows the 9 last observation of the current session.
class sessionTracker extends React.Component {
    
    
    constructor(props) {
        super(props);
        this.chart = new Charts({urlAPI:props.urlAPI});
        this.urlAPI=props.urlAPI;
        //We initialize the state to some slashes, it's just to be prettier on screen than nothing,
        //we also could put nothing here and put a condition in the render method for each obs.
        this.state = {
            obs:[],
        }
        
    }

    /**
     * This method is executed only when the component is Mounted on the page.
     * This is used to avoid initialization errors.
     */
    componentDidMount() {
        let currentComponent = this;
        //We get the current sessionId from the props, App.js give it to him each time it generates a new session.
        let urlApi = this.urlAPI+'/get/obsSessionId/' + this.props.sessionId;

        axios.get(urlApi)
            .then(async function (response) {
                let data = response.data;
                if(data===undefined || !Array.isArray(data)){
                    return;
                }
                data.sort((a, b) => {
                    //It is a custom sort function, we compare dates to sort observation in the right order.
                    if (a.observationDate < b.observationDate) return 1;
                    if (a.observationDate > b.observationDate) return -1;
                    return 0;
                })
                //We set the state to the right values if necessary, otherwise we don't
                let i = 0;
                while (i <= data.length - 1 && i < 9) {
                    var array=[...currentComponent.state.obs];
                    array.push(data[i].target.object_name)
                    await currentComponent.setState({obs:array});
                    i++;
                }
               
            })
            .catch(function (error) {
                console.log(error);
            });
            
    }

    /**
     * This method transform an idSession format yyyymmdd_hhmmss to the human_readable yyyy/mm/dd hh:mm:ss
     */
    sessionIdPrettyFormat = () => {
        let date = this.props.sessionId;
        return this.chart.dateFormat(date, "y/m/d");
    }

    /**
     * getvaluesFromSession : initializes the session id if it is null
     */
    getvaluesFromSession = () => {
        var s = document.getElementById("sessionId");
        if(s.innerHTML === ""){
            let date = this.props.sessionId;
            var sessionID = date.replace('_', '');
            s.innerHTML = "SessionID : "+ sessionID;
        }else{
            s.innerHTML = "";
        }
    }

    render() {
        const sessionTracker=window["translations"]().title.sessionTracker;
        return (
            <div className={"session"}>
                <p className={"text-center mb-1 font-weight-bold"}>{sessionTracker}<br/></p>
                <p>{this.sessionIdPrettyFormat()}</p>
                <button className={"btn btn-warning text-white btn-get btn-get-id"} type="button" onClick={this.getvaluesFromSession}>Get values from session ID</button>
                <p id="sessionId"></p>
                <hr/>
                {/* The values in curly braces are refreshed in real time when the state is updated */}
                {this.state.obs.map((element,i)=>{
                    return(<p key={i}>┣━━━━ {element}</p>); 
                })}
            </div>
        );
    }
}

export default sessionTracker;

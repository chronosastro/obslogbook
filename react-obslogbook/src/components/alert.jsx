import * as React from 'react';
import axios from 'axios';
import AlertTable from './alertTable';
import 'antd/dist/antd.css';
import './css/alert.css'; 
import {Container,Row,Button} from'react-bootstrap';
import { Col } from 'antd';
import { faSyncAlt} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

/**
This class is the one displayed for the search tab of the application, we use each sub component such as observationResult.jsx and charts.jsx here
*/
class search extends React.Component {
    constructor(props) {
		super(props);
		this.urlAPI=props.urlAPI;
		this.urlAlert=props.urlAlert;
		this.tableDisp=React.createRef();
		this.charts=React.createRef();
        this.state = {
			filterValue:'object_name',
			typeValue:'',
			cooldown:false,
			timeout:"",
			listAlert:[],
			alerts:[]
		}

		this.getAllAlerts(this.setAlert);
		
	}

	getAllAlerts = (callback) =>{
		var comp = this;
		axios.defaults.headers.get['Content-Type'] ='application/json;charset=utf-8';
        axios.defaults.headers.get['Access-Control-Allow-Origin'] = '*';
        axios.get("https://spectrotom-cors.herokuapp.com/"+this.urlAlert+"/api/targets/?format=json")
		.then(async function (response) {
			comp.setState({
				...comp.state,
				listAlert: response.data.results
			});
			callback(comp.convertAll(response.data.results));
		})
		.catch(async function (error) {
			console.log(error);
		});
	}

	convertAll = (res)=>{
		function degToHMS(ra,dec){
			//dec
			var ds='';
			var rs='';
			if(dec<0){
				dec = Math.abs(dec);
				ds='-';
			}
			var deg = parseInt(dec);
			var decM = Math.abs(parseInt((dec-deg)*60));
			var decS = (Math.abs((dec-deg)*60)-decM)*60;
			var DEC=`${ds}${deg} ${decM} ${decS}`;

			//ra
			if(ra<0){
				ra=Math.abs(ra);
				rs='-';
			}
			var raH=parseInt(ra/15);
			var raM=parseInt(((ra/15)-raH)*60);
			var raS=((((ra/15)-raH)*60)-raM)*60
			var RA=`${rs}${raH} ${raM} ${raS}`;
			
			return [RA,DEC];
		}
		var resConv=[];
		res.forEach((alert)=>{
			var ra=alert['ra'];
			var dec=alert['dec'];
			var conv=degToHMS(ra,dec);
			alert['ra']=conv[0];
			alert['dec']=conv[1];
			resConv.push(alert);
		});
		return resConv;
	}

	setAlert = (list) =>{
		this.setState({alerts:list});
	}
	
	refresh = () =>{
		this.getAllAlerts(this.setAlert)
	}

	redirectToTargets = () =>{
		var win = window.open(this.urlAlert+"/targets/", '_blank');
		win.focus();
	}

	/**
	First we defined antd Table columns (see:https://ant.design/components/table/)
	Then depending on the state.filterValue value we render a specific form.
	For exemple if "type" is selected we display a Select form. Fo reach filter except Type, there is also a select for the observation's type
	*/
    render() {	
		var title=window["translations"]().title;
		return (
			<Container className="w-100 mw-100">
				<Row className="d-flex justify-content-center" >
					<div className={"alertHeader w-75"}>
						<h5 className={"text-center mb-1 alertTitle"}><br/>{title.alert}</h5>
						<hr className={"rounded"}/>
						<label className="text-center ml-4 mt-3 w-100 ">
							<Row className="w-100 d-block" >
								<Col className="alertCol w-100" >
									<span className={"text-light"}>{title.totalAlert}</span>
									<span className={"text-light"}>{this.state.alerts.length}</span>
									<Button id="alertButton" onClick={this.refresh} className="purple-bg refreshButton"><FontAwesomeIcon icon={faSyncAlt}/></Button>
								</Col>
							</Row>
							<hr id="delimiter"/>
							<Row className="w-100 d-block" >
								<Col className="alertCol w-100" >
									<Button id="goToTarget" onClick={this.redirectToTargets} className="purple-bg w-50 mt-3">{title.addALert}</Button>
								</Col>
							</Row>
						</label>	
						<hr id="separator"/>					
					</div>
				</Row>

				<Row className="d-flex justify-content-center">
					<div className={"alertBody w-75 ml-5 mr-5"}>
						<AlertTable alert={this.getAllAlerts}/>
						<hr className={"rounded"}/>	
					</div>
				</Row>
		</Container>
		);
		
				
    }
}
export default search;

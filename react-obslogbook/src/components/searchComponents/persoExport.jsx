import * as React from 'react';
import 'antd/dist/antd.css';
import '../css/persoExport.css'; //changer le CSS


import {Col,Form, Dropdown} from 'react-bootstrap';
import TableData from './tableData'



import {PDFDownloadLink } from '@react-pdf/renderer';
import PDFmaker from './PDF/PDFmaker';



class persoExport extends React.Component {
    
    
    constructor(props) {
		super(props);
        this.title=window['translations']().title;
        this.observation=JSON.parse(JSON.stringify(props.observation));
        this.fileName = props.observation["_id"]+"obs.pdf";
        this.key=0;
        this.numId = 0;
        this.state={
            observation:props.observation,
            flatMode:false,
            //Component list
            component_list:null,
            component_ref:[]
        }
        this.handleChange = this.handleChange.bind(this);
    }

    copyToClipboard = async () =>{
        
        var data = this.state.observation;

		var JSONvalue=JSON.stringify(data,null,'\t')
		// Create an input to copy the string array inside it
		var textToCopy = document.createElement("textarea");
		

		// Add it to the document
		document.body.appendChild(textToCopy);

		// Set its ID
		textToCopy.setAttribute("id", "textToCopy_id");

		// Output the array into it
		document.getElementById("textToCopy_id").value=JSONvalue
		// Select it
		textToCopy.select();
		//For mobile devices
		textToCopy.setSelectionRange(0, 99999);

		// Copy its contents
		document.execCommand("copy");
		document.body.removeChild(textToCopy);
	}

    downloadFile = async () =>{
        var data = this.state.observation;
		var JSONvalue=JSON.stringify(data,null,'\t')
		// Create a input to copy the string array inside it
		var textToCopy = document.createElement("textarea");

		// Add it to the document
		document.body.appendChild(textToCopy);

		// Set its ID
		textToCopy.setAttribute("id", "textToDownload_id");

		// Output the array into it
		document.getElementById("textToDownload_id").value=JSONvalue
		const element = document.createElement("a");
		const file = new Blob([document.getElementById('textToDownload_id').value], {type: 'text/plain'});
		element.href = URL.createObjectURL(file);
		//Name of the downloaded file
		element.download = this.observation.observation_id;
		document.body.appendChild(element); // Required for this to work in FireFox
		element.click();
		document.body.removeChild(textToCopy);
		document.body.removeChild(element);
		

	}   
    /**
     * Set component list with checkbox on first initialization in the state for counter the refresh of state
     */
    componentDidMount(){
        this.setState({component_list:this.renderFunction(this.props.observation,0)});
    }
    /***
     * Create components in the state
     */
    renderFunction=(obj,padding)=>{
        var title=window["translations"]().title;
        var ret=[];
        
        // For each value in the array of observation's data
        Object.keys(obj).forEach(info=>{
            var cname="pl-"+padding;
            var jump=<hr key={this.key++}  className='rounded'></hr>;
            const rowLen=Object.keys(obj[info]).length;

            // Verifiaction if the value is not empty
            if(obj[info] !== ""){
                var ref= React.createRef();
                this.state.component_ref.push(ref);
                ret.push(
                    <div className={cname+" div_check"} key={this.key++} id="all" >											
                        {typeof(obj[info])==="string"&&<Form.Check onChange={this.obsState}ref={ref} id={info} className="check" type="checkbox" label={title[info]!==undefined?title[info]:info} defaultChecked="true" />}
                        {typeof(obj[info])==="string"?
                            <span id={"val_"+info} className="pl-2 val_check" key={this.key++} >{obj[info]}</span>
                            :ret.push(<h6 key={this.key++} className="font-weight-bold mt-2" id="greattitle">
                                {title[info]!==undefined?this.renderFunction(obj[info],padding+2).length!==0&&title[info]+" :":info+" :"}
                                {Object.keys(obj[info]).length!==0?jump:null}</h6>)&&
                                this.renderFunction(obj[info],padding+2).forEach((x,i)=>{
                                    if(rowLen===i+1){
                                        ret.push(x)
                                        ret.push(jump)
                                    }
                                    else{
                                        ret.push(x)
                                    }
                                })
                                }
                                
                    </div>);
                    this.numId++; 
            }
        });
        return ret;
    }

    /**
     * Refresh the observation state
    */
    obsState=()=>{
        var data = {}; 
        var id; 
        var val; 

        // For each element, verification if it's checked or not
        this.state.component_ref.forEach(element =>{
            // Verification if the element is not null
            if(element.current !== null){
                // Verifiaction if the element is checked
                if(element.current.checked){
                    id = element.current.id;
                    val = document.getElementById("val_"+id).innerHTML; 
                    data[id] = val;
                }
            }
        })
        this.setState({observation:data,flatMode:true});
    }
    
    /**
     * Select and unselect all function
     */
    changeAll = ()=>{
        var checkbox = document.getElementById("checkAll");
        
        // Verification if the checkbox is not null
        if(checkbox !== null){

            //For each element, select or unselect the checkbox
            this.state.component_ref.forEach(element => {
                if(element.current !== null){
                    if(!checkbox.checked)
                        element.current.checked = false; 
                    else
                        element.current.checked = true; 
                }
            })
        }
        // Refresh the observation state
        this.obsState();
    }

    /**
     * handleChange : change the state associate with the insert value
     * @param {*} target : the target that using the function
     */
    handleChange = ({target}) => {
        const {id,value}=target;
        this.setState({[id]: value});
        console.log(this.state);
    }


    render(){
        var title=window["translations"]().title;
        var button=window["translations"]().button;
        return( 
        <div>
            <TableData observation={this.state.observation} id_table="table-to-xls-perso"/>
            <div className="exportObs" >
                <Form.Group>
                <Form.Check id="checkAll" type="checkbox" label="Select all" defaultChecked="true" onChange={this.changeAll} />
                <hr/>
                <div>	
                    <Col className="textLabel">
                        <span className="pl-2 val_check" key={this.key++}>{title.introduction}</span>
                    </Col>	
                    <Col className="col_text">
                        <textarea cols="72" className="textArea" id="intro" value={this.state.intro} onChange={this.handleChange}/>
                    </Col>
                </div>
                {this.state.component_list}
                <div>
                    <Col className="textLabel">
                        <span className="pl-2 val_check" key={this.key++}>{title.conclusion}</span>
                    </Col>	
                    <Col className="col_text">
                        <textarea cols="72" id="concl" className="textArea" value={this.state.concl} onChange={this.handleChange} />
                    </Col>
                </div>
                </Form.Group>
                <Dropdown className="mt-2 mr-1 export-dropdown" title={title.selectExport}>
						<Dropdown.Toggle className="btn-purple mt-2 mr-1" >{button.selectExport}</Dropdown.Toggle>

						<Dropdown.Menu>
                            <PDFDownloadLink className={"exportItem"} document={ 
                                <PDFmaker
                                flatMode = {this.state.flatMode}
                                observation = {this.state.observation}
                                addons = {[this.state.intro,this.state.concl]}
                                />  
                            } fileName={this.fileName}>
								{({ blob, url, loading, error }) => (loading ? this.title.loadingDocument : "PDF" )}
							</PDFDownloadLink>
                            <Dropdown.Item id="word" className="exportItem" onClick={()=>{require("../classes/word").default(this.state.observation)}}>{"Word"}</Dropdown.Item>
                            <Dropdown.Item id="excel" className="exportItem" onClick={()=>{require('../classes/excel').default("table-to-xls-perso",this.props.observation.observation_id)}}>{"Excel"}</Dropdown.Item>
                            <Dropdown.Item id="text" className="exportItem" onClick={()=>{this.downloadFile()}}>{"Text"}</Dropdown.Item>
							<Dropdown.Item id="clipboard" className="exportItem" onClick={({target})=>{this.copyToClipboard();target.innerHTML=this.title.copyDone}}>{"Clipboard"}</Dropdown.Item>
						</Dropdown.Menu>

					</Dropdown>
            </div>
        </div>
        ); 
    }





}
export default persoExport;
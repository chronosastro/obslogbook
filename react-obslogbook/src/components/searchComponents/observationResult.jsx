import * as React from 'react';
import "../css/observationResult.css";


/**
This class is used to render all observation info in search tab when the user click on Display for one of the observation.
In render() we use Object.keys() to get all the element of a level in the json. for each element we check if it's a string so we can display it or if it's an Object.
If it's an object that means it's a nested information so we have to repeat the process explain above.
*/
export default class ObservationResult extends React.Component {
		constructor(props){
			super(props);
			this.key=0;
		}

		/**
		 * renderFunction : diplay the result of the observation
		 * @param {*} obj , array of observation's data
		 * @param {*} padding , the padding used to the display
		 */
		renderFunction=(obj,padding)=>{
			var title=window["translations"]().title;
			var ret=[];
			// For each data in the array of observation's value
			Object.keys(obj).forEach(info=>{
				var cname="pl-"+padding;
				var jump=<hr key={this.key++}  className='rounded'></hr>;
				const rowLen=Object.keys(obj[info]).length;
				// Verification if the value is not empty
				if(obj[info] !== ""){
					ret.push(
						<div className={cname} key={this.key++} id="all">											
							{typeof(obj[info])==="string"&&<span className="font-weight mt-2" id="title">{title[info]!==undefined?title[info]:info} : </span>}
							{typeof(obj[info])==="string"?
								<span key={this.key++} >{obj[info]}</span>
								:ret.push(<h6 key={this.key++} className="font-weight-bold mt-2" id="greattitle">
									{title[info]!==undefined?this.renderFunction(obj[info],padding+2).length!==0&&title[info]+" :":info+" :"}
									{Object.keys(obj[info]).length!==0?jump:null}</h6>)&&
									this.renderFunction(obj[info],padding+2).forEach((x,i)=>{
										if(rowLen===i+1){
											ret.push(x)
											ret.push(jump)
										}
										else{
											ret.push(x)
										}
									})
									}
									
						</div>);
				}
			})
			return ret;
		}

		render(){
			return(
				<div>
					<div className="obs" >
						{this.renderFunction(this.props.observation,0)}
					</div>
				</div>
			)
		}
}

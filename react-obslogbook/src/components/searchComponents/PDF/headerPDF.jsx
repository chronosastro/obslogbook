import React from 'react';
import {Text, View,StyleSheet,Image} from '@react-pdf/renderer';
import logo from "../../../resources/logoOLB.png";

/**
 * headerPDF : return a header PDF Document
 */
function headerPDF(props) { 
    
    // Constant of the styles used in the PDF header
    const styles = StyleSheet.create({
        container: {
            flexDirection: 'row',
            borderBottomWidth: 2,
            borderBottomColor: '#112131',
            borderBottomStyle: 'solid',
            alignItems: 'stretch',
        },
        detailColumn: {
            flexDirection: 'column',
            flexGrow: 9,
            textTransform: 'uppercase',
        },
        linkColumn: {
            flexDirection: 'column',
            flexGrow: 2,
            alignSelf: 'flex-end',
            justifySelf: 'flex-end',
        },
        name: {
            fontSize: 24,
        },
        subtitle: {
            fontSize: 10,
            justifySelf: 'flex-end',
        },
        link: {
            fontSize: 10,
            color: 'black',
            textDecoration: 'none',
            alignSelf: 'flex-end',
            justifySelf: 'flex-end',
        },
        image: {
            justifySelf: 'flex-end',
        }
    });

    return(
        <View style={styles.container}>
            <Image style={styles.image} src={logo}/>
            <View style={styles.detailColumn}>
                <Text style={styles.name}>{props.nameObject}</Text>
                <Text style={styles.subtitle}>{props.type}</Text>
                <Text style={styles.subtitle}>RA: {props.decTarget}</Text>
                <Text style={styles.subtitle}>DEC: {props.raTarget}</Text>
            </View>
            <View style={styles.linkColumn}>
                <Text style={styles.link}>ObsLogBook</Text>
            </View>
        </View>
    );
}

export default headerPDF;
import React from 'react';

/**
 * TableData : create a table with observation's data
 */
export default function TableData(props){
        var title=window['translations']().title;
		var datas = [];
		var datasolo=[];
		var observation=props.observation;
		var key=0;

		// For each value in the observation
		Object.keys(observation).forEach(info => {
			// Verification if the value is not empty
			if (observation[info] !== "") {

				// Verification if the value is an object or a text
				if (typeof(observation[info])==="object") {
					// If the value is an object that means, it's a category of values
					// so the name of the value category is diplay in title caraters 
					datas.push(
						<tr key={key++}>
							<th colSpan={2}>{title[info]}</th>
						</tr>
					);
					// And its sub-values are diplayed under 
					Object.keys(observation[info]).forEach(data => {
						if (observation[info][data] !== "") {
							datas.push(
								<tr key={key++}>
									<td>{title[data]}</td>
									<td>{observation[info][data]}</td>
								</tr>
							)
						}
					})
					datas.push(<tr key={key++}></tr>);
				} else {
					if(info==='key')return;
					datasolo.push(
						<tr key={key++}>
							<td>{title[info]}</td>
							<td>{observation[info]}</td>
						</tr>
					);
				}
				
			}
		});
		return (
			<table id={props.id_table} hidden>
				<tbody>
					<tr><th colSpan={2}>{title.observationExcel}</th></tr>
					{datasolo}
					<tr></tr>
					{datas}
				</tbody>
			</table>
		)
	}
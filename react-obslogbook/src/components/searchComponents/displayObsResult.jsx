import React, {useEffect, useState} from 'react';

import TableData from './tableData';
import ObservationResult from './observationResult';
import PDFmaker from './PDF/PDFmaker';
import "../css/displayObsResult.css";
import {PDFDownloadLink } from '@react-pdf/renderer';
import {Dropdown} from'react-bootstrap';
import PersoExport from "./persoExport";

/**
This class is an element of search components (display obs result)
*/
function DisplayObsResult(props){

	var title=window["translations"]().title;
	var button=window["translations"]().button;

	const fileName = props.observation["_id"]+"obs.pdf";

	useEffect(()=>{
		if(document.getElementById("myNav")!=null){
			document.getElementById("myNav").style.height = "100%";
			document.getElementById("myNav").style.width = "100%";
		}
	});
	/**
	This function is used for the copy button, when the user click on the button, we create an textarea, set the value to the observation's information
	and then select and  copy the textarea value with document.execCommand("copy"). After we remove the textArea and change button text to "Copied !"
	*/
	let copyToClipboard = async () =>{
		// Create an input to copy the string array inside it
		var textToCopy = document.createElement("textarea");

		// Add it to the document
		document.body.appendChild(textToCopy);

		// Set its ID
		textToCopy.setAttribute("id", "textToCopy_id");

		// Output the array into it
		document.getElementById("textToCopy_id").value=JSON.stringify(props.observation,null,'\t');
		// Select it
		textToCopy.select();
		//For mobile devices
		textToCopy.setSelectionRange(0, 99999);

		// Copy its contents
		document.execCommand("copy");
		document.body.removeChild(textToCopy);
	}

	/**
	This function is used for the download button, when the user click on the button, we create an textarea, set the value to the observation's information
	and then download it.
	*/
	let downloadFile = async () =>{
		const element = document.createElement("a");
		const file = new Blob([JSON.stringify(props.observation,null,'\t')], {type: 'text/plain'});
		element.href = URL.createObjectURL(file);
		//Name of the downloaded file
		element.download = props.observation.observation_id;
		document.body.appendChild(element); // Required for this to work in FireFox
		element.click();
		document.body.removeChild(element);
	}

	const [displayMode,setDisplayMode]=useState(0);
	/**
	This function is here in order to reset button text when we change the displayed observation.
	Each time the prop given by search.jsx is changed we reset the state.
	*/
	return(
		
		props.observation!=="" &&				
		<div id="myNav" className="overlay">
			<div id="myNavBar">
				<TableData observation={props.observation} id_table="table-to-xls"/>
				<a href="/#" className="closebtn" onClick={() => {
					props.hide();
					if(document.getElementById("myNav")!==null){
						document.getElementById("myNav").style.height = "0%";
						document.getElementById("myNav").style.width= "0%";
					}}}>&times;</a>
				<div id="divBt">
					<Dropdown className="mt-2 mr-1 export-dropdown" title={title.quickExport}>
						<Dropdown.Toggle className="btn-purple mt-2 mr-1" >{button.quickExport}</Dropdown.Toggle>

						<Dropdown.Menu>
							<PDFDownloadLink className={"exportItem"} document={<PDFmaker observation = {props.observation} addons = {[null, null]}/>} fileName={fileName} >
								{({ blob, url, loading, error }) => (loading ? title.loadingDocument : "PDF" )}
							</PDFDownloadLink>
							<Dropdown.Item id="word" className="exportItem" onClick={()=>{require("../classes/word").default(props.observation)}}>{"Word"}</Dropdown.Item>
							<Dropdown.Item id="excel" className="exportItem" onClick={()=>{require('../classes/excel').default('table-to-xls',props.observation.observation_id)}}>{"Excel"}</Dropdown.Item>
							<Dropdown.Item id="text" className="exportItem" onClick={()=>{downloadFile()}}>{"Text"}</Dropdown.Item>
							<Dropdown.Item id="clipboard" className="exportItem" onClick={({target})=>{copyToClipboard();target.innerHTML=title.copyDone}}>{"Clipboard"}</Dropdown.Item>
						</Dropdown.Menu>

					</Dropdown>
					<button className="btn mt-2 mr-1 bt " title={displayMode===0?title.persoExport:title.obsResult} onClick={()=>{displayMode===0?setDisplayMode(1):setDisplayMode(0)}}>{displayMode===0?button.persoExport:button.obsResult}</button>
				</div>
			</div>
			<div className="overlay-content">
				{
				displayMode===0?
					<ObservationResult observation={props.observation}/>
					:
					<PersoExport observation={props.observation}/>
				}	
			</div>
		</div>

		);

		
}

export default DisplayObsResult;
import * as React from 'react';
import './css/Aladin.css';
import {Card} from 'react-bootstrap';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';



class Aladin extends React.Component {
    /**
     * @deprecated Handle the creation of the Aladin map and the placement to the right coordinates
     */
    handleSkyMapCreation = (rra,dec) => {
        //last part of this function will create the sky map
        var aladin = window.A.aladin('#aladin-lite-div',
            {
                survey: 'P/allWISE/color', // set initial image survey
                fov: 1.5, // initial field of view in degrees
                cooFrame: 'j2000', // set galactic frame
                reticleColor: '#ff89ff', // change reticle color
                reticleSize: 64 // change reticle size
            });
        //we convert ra into the correct format, indi returns decimal hours while aladin require classic format "hour min sec"
        var ra = rra.toString().split(".")
        var hours = ra[0]
        var min = "0." + ra[1]
        min = min * 60
        var minIntegerPart = Math.trunc(min)
        var sec = min.toString().split(".")
        //Require to avoid 1 decimal number problem, can't use .toFixed because it doesn't add 0
        sec = sec[1] * 10
        sec = "0." + sec
        sec = sec * 60
        var coord = hours + " " + minIntegerPart + " " + sec + " +" + dec;
        //then set aladin map's coordinates to coord
        aladin.gotoObject(coord)
    }

    /** Set the current aladin frame target with coordinates @param ra and @param dec  */
    handleSkyMapTarget = (ra,dec) => {
        //Go to coords ra dec
        this.aladin.gotoObject(ra+" "+dec)
    }
    componentDidMount(){
        var nameScript='script'+this.props.observationId;
        nameScript=nameScript.replace(/\s/g, '');
        window[nameScript]= document.createElement('script');
        window[nameScript].type = 'text/javascript';
        window[nameScript].src = "https://aladin.u-strasbg.fr/AladinLite/api/v2/latest/aladin.min.js";
        window[nameScript].id=nameScript;
        document.head.appendChild(window[nameScript]);
        document.getElementById(nameScript).addEventListener('load',()=>{
           
            this.aladin = window.A.aladin('#aladin-lite-div',
            {
                survey: 'P/DSS2/color', // set initial image survey
                fov: 1, // initial field of view in degrees
                cooFrame: 'j2000', // set galactic frame
                reticleColor: '#7A64F7', // change reticle color
                reticleSize: 64 // change reticle size
            });
            })
    }

    render(){
        var trad = window["translations"]().info;
        return (
            <div className="w-100">
                <div id='aladin-lite-div' style={{ width: "100%",height:"25vw"}} />
                <Tabs id="tab" className="justify-content-center corner">
                    <Card>
                        <TabList id="id">
                            <Tab className="tab_tr" tabFor="tar">{trad.title_target}</Tab>
                            <Tab className="tab_tr" tabFor="ref" disabled={this.props.visibleRefStar}>{trad.title_ref_star}</Tab>
                        </TabList>
                    </Card>
                    <Card id="card">
                        <TabPanel tabId="tar">
                            <Card.Body className="environmentTabContent">
                                <div id='target_info'>
                                    <table id="atable">
                                        <tbody>
                                        <tr id="atr">
                                            <td id="atd">{trad.rv_value}</td>
                                            <td id='rv_value_info'></td>
                                        </tr>

                                        
                                        <tr id="atr">
                                            <td id='title_sptype_info'>{trad.sptype}</td>
                                            <td id='sptype_info'></td>
                                        </tr>
                                        
                                        <tr id="atr">
                                            <td id='title_mt_info'>{trad.mp_type}</td>
                                            <td id='mt_info'></td>
                                        </tr>
                                        
                                        <tr id="atr">
                                            <td id="atd">{trad.flux_v}</td>
                                            <td id='flux_v_info'></td>
                                        </tr>
                                        
                                        <tr id="atr">
                                            <td id="atd">{trad.otype}</td>
                                            <td id='otype_info'></td>
                                        </tr>
                                        
                                        <tr id="atr">
                                            <td id="atd">{trad.ra}</td>
                                            <td id='ra_info'></td>
                                        </tr>
                                        
                                        <tr id="atr">
                                            <td id="atd">{trad.dec}</td>
                                            <td id='dec_info'></td>
                                        </tr>
                                        
                                        <tr id="atr">
                                            <td id="atd">{trad.dim}</td>
                                            <td id='dim_info'></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>            
                            </Card.Body>
                        </TabPanel>
                    </Card>
                    <Card>
                        <TabPanel tabId="ref">
                            <Card.Body className="environmentTabContent">
                                <div id='ref_star_info'>
                                    <table id="atable">
                                        <tbody>
                                        <tr id="atr">
                                            <td id="atd">{trad.rv_value}</td>
                                            <td id='rs_rv_value_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id='title_rs_sptype_info'>{trad.sptype}</td>
                                            <td id='rs_sptype_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id='title_rs_mt_info'>{trad.mp_type}</td>
                                            <td id='rs_mt_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id="atd">{trad.flux_v}</td>
                                            <td id='rs_flux_v_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id="atd">{trad.otype}</td>
                                            <td id='rs_otype_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id="atd">{trad.ra}</td>
                                            <td id='rs_ra_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id="atd">{trad.dec}</td>
                                            <td id='rs_dec_info'></td>
                                        </tr>
                                        <tr id="atr">
                                            <td id="atd">{trad.dim}</td>
                                            <td id='rs_dim_info'></td>
                                        </tr>
                                        </tbody>
                                    </table>              
                                </div>           
                            </Card.Body>
                        </TabPanel>
                    </Card>
                </Tabs>
            </div>
        )
    }
    
}export default Aladin
/**
 * createExcel : generate an Excel file with the observation's datas
 * @param table_id , the table name of datas
 * @param excel_name , the name of the Excel file
 */
export default function createExcel (table_id,excel_name) {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(table_id);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    excel_name = excel_name!==undefined?excel_name+'.xls':'dataObs.xls';//modify excle sheet name here 
    // Create download link element
    downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, excel_name);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
        // Setting the file name
        downloadLink.download = excel_name;
        //triggering the function
        downloadLink.click();
    }
}
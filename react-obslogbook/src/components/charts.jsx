import * as React from 'react';
import axios from 'axios';
import Plotly from "plotly.js-basic-dist";
import createPlotlyComponent from "react-plotly.js/factory";
import moment from 'moment'

const Plot = createPlotlyComponent(Plotly);

/**
This class is used to render charts using Plotly, those charts are displayed in search.jsx
*/
export default class Charts extends React.Component {
		
	constructor(props) {
		super(props);
		this.urlAPI=props.urlAPI;
		this.state = {
				nbTypeObservation:[],
				nbObservationByYear:[],
				years:[],
				nbObs: null,
				totalExposure: 0,
				first: { 
					"_id" : "//",
					"num_obs" : "//"
				},
				second: { 
					"_id" : "//",
					"num_obs" : "//"
				},
				third: { 
					"_id" : "//",
					"num_obs" : "//"
				}
		}
	}

	/**
	The date stored in the database is not in a proper format for librairie so we have to convert it.
	With this function we divide the string to get each part of the date and then make a string with a proper format
	*/
	dateFormat = (date, format) => {
		let dateFormat = "";

		// Each element of the date is separated from the others
        let year = date.substring(0, 4);
        let month = date.substring(4, 6);
        let day = date.substring(6, 8);

		// Elements are reassemebled in the format in parameters
		if(format === "y-m-d"){
			dateFormat = year + "-" + month + "-" + day;
		}
		else if(format === "y/m/d"){
			dateFormat = year + "/" + month + "/" + day;
		}
		else if(format === "ymd"){
			dateFormat = year + "" + month + "" + day;
		}

        return dateFormat.toString()
    }

		/**
		Use for the pie charts of repartition of observation's type
		*/
		countType = (observations) =>{
			var count=[0,0,0]

			// Each observation is classed in the table by it type
			for (var obs of observations){
				switch (obs.observation_type) {
					case "visual":
						count[0]++
						break;
					case "astro":
						count[1]++
						break;
					case "spectro":
						count[2]++
						break;
					default:
						break;
				}
			}
			return count
		}

		/**
		Use for the number of observation per year charts, for each observations in we check the year using moment then we add 1 to the dict with a key = year
		so we return the count but also of years
		*/
		countYear = (observations) =>{
			var count={}

			for (var obs of observations){
				// Verification if the observation is not undefined and if it date is not undefined
				if(obs===undefined || obs.date===undefined){
					return;
				}
				var year=moment(obs.date.slice(0,8)).year();
				if(count[year]!==undefined){
					count[year]+=1
				}
				else {
					count[year]=1
				}
				var yValues=[];
				var years=Object.keys(count);
				
				var i=0
				for(var y of years){
					yValues[i]=count[y]
					i++
				}
			}
			var ret=[years,yValues]
			return ret
		}

		/**
		We get all observations in the database and call each function defined above to set the state and use the state to render charts
		*/
		componentDidMount(){

			// Initialization of the charts about the total of observation, the most popular and the total of observation 
			this.getTotalObs();
			this.getTheMostPopular();
			this.getTotalExposure();

			var component = this;
			axios.get(this.urlAPI+"/get/obs/")
			.then(async function (response) {
				var countObsByType=component.countType(response.data)
				var countYears=component.countYear(response.data)
				if(countYears===undefined){
					return;
				}
				component.setState({
					nbTypeObservation: countObsByType,
					nbObservationByYear:countYears[1],
					years:countYears[0]
				});
			})
			.catch(async function (error) {
				console.log(error);
			});
		}

		/**
		 * initializes the total number of observations
		 */
		getTotalObs() {
			var component = this
			axios.get(this.urlAPI+"/get/totalobs/")
			.then(async function (response) {
				var nb = response.data;
				component.setState({
					nbObs: nb
				});
			})
			.catch(async function (error) {
				console.log(error);
			});
		}

		/**
		 * give the three most popular objects observed
		 */
		getTheMostPopular() {
			var component = this
			axios.get(this.urlAPI+"/get/mostpopular/")
			.then(async function (response) {
				if ( response.data[0] !== undefined) {
					component.setState({
						first: response.data[0]
					});
				}
				if ( response.data[1] !== undefined) {
					component.setState({
						second: response.data[1]
					});
				}
				if ( response.data[2] !== undefined) {
					component.setState({
						third: response.data[2]
					});
				}
			})
			.catch(async function (error) {
				console.log(error);
			});
		}

		/**
		 * give the total of exposition time of all observations
		 */
		getTotalExposure() {
			var component = this
			axios.get(this.urlAPI+"/get/totalexposure/")
			.then(async function (response) {
				var list = response.data;
				var total = 0;

				// With each value, the sum of the time exposure multiplied by the number of image is calculated to find the total time of exposure
				for (var i=0; i < list.length; i++) {
					if ( (list[i].target_image!==undefined) && (list[i].target_image.image_count !== undefined) && (list[i].target_image.image_exposure !== undefined) ) {
						var nbImage = parseFloat(list[i].target_image.image_count);
						var timeExpo = parseFloat(list[i].target_image.image_exposure);
						var calcul = nbImage*timeExpo;
						total = total + calcul;
					}
				}
				component.setState({
					totalExposure: total
				});
			})
			.catch(async function (error) {
				console.log(error);
			});
		}

		/**
		To make other type charts see: https://plotly.com/javascript/
		*/
		render() {
			var title=window["translations"]().title;
			var charts=window["translations"]().charts;
			return (
				<div className="w-100 justify-content-center charts">
			      <Plot
						data = {[{
							type: 'pie',
							values: this.state.nbTypeObservation,
							labels: [title.observation,title.astrophoto,title.spectrograph],
							chartArea: {
								backgroundColor: 'rgba(251, 85, 85, 0.4)'
							}
						}]}
						layout={ {
							width: 320, height: 240,
							title: title.charts1,
							plot_bgcolor:"rgba(255, 0, 0, 0)",paper_bgcolor:"rgba(255, 0, 0, 0)",
							font: {
								
								size: 10,
								color: 'White'
							  }
							} 
						
						}
							
			      />
						<Plot
							data={[
								{
									x: this.state.years,
									y: this.state.nbObservationByYear,
									type: 'scatter',
									mode: 'lines',
									name: 'Sommeil'
								}]}
				        layout={ {
							xaxis:{type:"category"}, 
							width: 320, height: 240, 
							title: title.charts2,
							plot_bgcolor:"rgba(255, 0, 0, 0)",paper_bgcolor:"rgba(255, 0, 0, 0)",
							font: {
								
								size: 10,
								color: 'White'
							  }
						} }
			      />
				  <div className="mt-3" id="info_obs">
						<table id="ranking">
							<thead>
								<tr><th className="text-black th-purple-bg" colSpan="3">{charts.nb_obs}{this.state.nbObs}</th></tr>
								<tr><th className="text-black th-purple-bg" colSpan="3">{charts.exposure}{this.state.totalExposure}s</th></tr>
								<tr id="ranking_desc">
									<th>{charts.ranking}</th>
									<th>{charts.obj_name}</th>
									<th>{charts.num_obs}</th>
								</tr>
							</thead>
							<tbody>
								<tr id="ranking_first">
									<td>1</td>
									<td>{this.state.first['_id']}</td>
									<td>{this.state.first['num_obs']}{charts.obs}</td>
								</tr>
								<tr id="ranking_second">
									<td>2</td>
									<td>{this.state.second['_id']}</td>
									<td>{this.state.second['num_obs']}{charts.obs}</td>
								</tr>
								<tr id="ranking_third">
									<td>3</td>
									<td>{this.state.third['_id']}</td>
									<td>{this.state.third['num_obs']}{charts.obs}</td>
								</tr>
							</tbody>
						</table>
					</div> 
				</div>
	    );

		}
}

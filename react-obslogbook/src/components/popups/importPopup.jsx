import * as React from 'react';
import Popup from 'reactjs-popup';
import '../css/importpopup.css';
import { Button } from'react-bootstrap'; 
import axios from 'axios';
import '../css/popup.css'; 

/**
 * this class is used when the user want to import a json database in the database of the site.
 */
class importPopup extends React.Component {

    constructor(props) {
        super(props);
        const importMessage = window["translations"]().import_message;
        this.urlAPI=props.urlAPI;
        this.state = {
            datatext: "",
            errorMsg: "",
            stateMsg: importMessage["stateMsg"]
        }
        this.click = this.click.bind(this);
        this.change = this.change.bind(this);
        this.dragover = this.dragover.bind(this);
        this.draglne = this.draglne.bind(this);
        this.drop = this.drop.bind(this);
    }

    /**
     * saveFile : save the data of the file in the drop area if it's a JSON file
     * @param {*} file , the file to save 
     */
    saveFile = (file) => {
        var current = this;
        // Creation of a filereader
        let reader = new FileReader();

        // The filereader read the file like a text and return the content into the state 'datatext'
        reader.readAsText(file);
        reader.onload = function(event) {
            current.setState({
                datatext: reader.result
            });
        }
    }

    /**
     * import : insert the data in the database
     */
    import = () => {
        const importMessage = window["translations"]().import_message;
        let current = this;
        // Verification if the datatext state is not empty
        if ( this.state.datatext !== "" ) {
            // Transformation of the textual data in datatext to a json data
            let jsondata = JSON.parse(this.state.datatext);

            // Preparation of the post request to insert many observations with the JSON file
            axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
            axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
            axios.post(this.urlAPI + '/post/multiforms', {
                forms: jsondata
            })
                .then(function() {
                    // If the request works, the success message is displayed and the error message is hidden
                    document.getElementById("errorMsgImport").hidden = true;
                    document.getElementById("successMsgImport").hidden = false;
                })
                .catch(function () {
                    current.setState({
                        errorMsg: importMessage["error3"]
                    });
                    // If the request doesn't work, the success message is hidden and the error message is displayed
                    document.getElementById("errorMsgImport").hidden = false;
                    document.getElementById("successMsgImport").hidden = true;
                });
        } else {
            this.setState({
                errorMsg: importMessage["error1"]
            });
            // The datatext state is empty so the error message is diplayed
            document.getElementById("errorMsgImport").hidden = false;
            document.getElementById("successMsgImport").hidden = true;
        }
    }

    /**
     * click : assign to the drop zone, allows you to drop a file in the same way as an input file
     */
    click = () => {
        document.getElementById("inputfile").click();
    }

    /**
     * change : used when the input file changes, replaces or adds the new uploaded file
     */
    change = () => {
        const inputfile = document.getElementById('inputfile');
        const dndzone = document.getElementById('dnd');

        // If there is a file in the inputfile element the diplay of the dropzone is updated and the file is saved in the state datatext
        if (inputfile.files.length) {
            this.updateThumbnail(dndzone, inputfile.files[0]);
            this.saveFile(inputfile.files[0]);
        }
    }

    /**
     * dragover : change the outline of the drop zone when the mouse hovers over it with a file
     */
    dragover = (e) => {
        const dndzone = document.getElementById('dnd');
        e.preventDefault();
        // A new class is add in the dropzone to change the diplay
        dndzone.classList.add("drop-zone--over");
    }

    /**
     * draglne (dragleave and dragend) : resets the normal contours when you stop hovering over the area with a file
     */
    draglne = () => {
        const dndzone = document.getElementById('dnd');
        // A class is remove to return at the original diplay
        dndzone.classList.remove("drop-zone--over");
    }

    /**
     * drop : prepare to save the file that is placed in the drop zone, change the display of the zone
     */
    drop = (e) => {
        e.preventDefault();
        const importMessage = window["translations"]().import_message;

        // The input file and the dropzone are save in a constante to used it
        const inputfile = document.getElementById('inputfile');
        const dndzone = document.getElementById('dnd');

        // Verification if a file is dropped
        if (e.dataTransfer.files.length) {
            inputfile.files = e.dataTransfer.files;
            // Verification if the file dropped is a JSON file
            if ( e.dataTransfer.files[0].type === 'application/json') {
                this.updateThumbnail(dndzone, e.dataTransfer.files[0]);
                this.saveFile(e.dataTransfer.files[0]);
                document.getElementById("errorMsgImport").hidden = true;
            } else {
                this.setState({
                    errorMsg: importMessage["error2"]
                });
                // The file is not a JSON file so the error message is displayed
                document.getElementById("errorMsgImport").hidden = false;
                document.getElementById("successMsgImport").hidden = true;
            }
        }
        dndzone.classList.remove("drop-zone--over");
    }

    /**
     * updateThumbnail : updates the display of the drop zone when a file is present
     */
    updateThumbnail = (dropZoneElement, file) => {
        let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

        // First time - remove the prompt
        if (dropZoneElement.querySelector(".drop-zone__prompt")) {
            dropZoneElement.querySelector(".drop-zone__prompt").remove();
        }

        //First time - there is no thumbnail element, so lets create it
        if (!thumbnailElement) {
            thumbnailElement = document.createElement("div");
            thumbnailElement.classList.add("drop-zone__thumb");

            dropZoneElement.appendChild(thumbnailElement);
        }

        thumbnailElement.dataset.label = file.name;
    }

    render() {
        const importMessage = window["translations"]().import_message;
        return (
            <Popup
                open={this.props.visible}
                closeOnDocumentClick
                onClose={this.props.closePopup}
                modal
            >
                <div>
                    <center>
                        <h1>{importMessage["title"]}</h1>
                        <p>{importMessage["message"]}</p>
                        <p>{importMessage["example"]}</p>
                        <div id="dnd" className="drop-zone" onClick={this.click} onDragOver={this.dragover} onDragLeave={this.draglne} onDragEnd={this.draglne} onDrop={this.drop}>
                            <span className="drop-zone__prompt">{importMessage["dropzone"]}</span>
                            <input id="inputfile" type="file" name="myFile" accept="application/json" className="drop-zone__input" onChange={this.change} />
                        </div>
                        <div id='spanMessage'>
                            <span id="errorMsgImport" className="disp" style={{ color: "red" }} hidden>{this.state.errorMsg}</span>
                            <span id="successMsgImport" className="disp" style={{ color: "green" }} hidden>{this.state.stateMsg}</span>
                        </div>
                        <Button onClick={this.import} className={"btn purple-bg" }>{importMessage["button"]}</Button>
                    </center>
                </div>
            </Popup>
        );
    }
}

export default importPopup;
import * as React from 'react';
import Popup from 'reactjs-popup';
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";

import '../css/popup.css'; 

/**
 * This class is used to generate the parameters popup
 */
function paramPopup(props) {
    const lang=props.lang;
    const param=window["translations"]().param;

    return (
        <Popup
            open={props.visible}
            closeOnDocumentClick
            onClose={props.closeParamPopup}
            modal
        >
            <div className="settingsPopup">
                <Tabs>
                    <TabList>
                        <Tab>{param["general"]}</Tab>
                        <Tab>{param["def_values"]}</Tab>
                    </TabList>
                    <TabPanel className="settingsGeneralContent">
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <span className="input-group-text">MongoDB URI</span>
                            </div>
                            <input type="text"
                                    name="mongoURI"
                                    className={"form-control"}
                                    placeholder={'mongodb://USER:PASS@HOST:PORT'}
                                    value={props.settings.mongoURI}
                                    onChange={props.handleSettingChange}/>   
                        </div>
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <span className="input-group-text">DB Name</span>
                            </div>
                            <input type="text"
                                    name="db_name"
                                    className={"form-control"}
                                    value={props.settings.db_name}
                                    onChange={props.handleSettingChange}/>  
                        </div>
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <span className="input-group-text">Table Name</span>
                            </div>
                            <input type="text"
                                    name="collection_name"
                                    className={"form-control"}
                                    value={props.settings.collection_name}
                                    onChange={props.handleSettingChange}/>  
                        </div> 
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <span className="input-group-text">Alert Broker URL</span>
                            </div>
                            <input type="text"
                                    name="urlAlert"
                                    className={"form-control"}
                                    placeholder={'https://APPLICATION_NAME.herokuapp.com'}
                                    value={props.settings.urlAlert}
                                    onChange={props.handleSettingChange}/>   
                        </div>
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <span className="input-group-text">{param["language"]}</span>
                            </div>
                            <select id="lang" defaultValue={lang}onChange={event=>{props.setLang(event.target.value)}}>
                                <option value="en">English</option>
                                <option value="fr">Français</option>
                                <option value="es">Español</option>
                            </select>
                        </div>
                    </TabPanel>
                    <TabPanel>
                        <Tabs>
                            <TabList>
                                <Tab>{param["environment"]}</Tab>
                                <Tab>{param["equipment"]}</Tab>
                            </TabList>
                            <TabPanel className="settingsEnvironmentContent">
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["obj_name"]}</span>
                                    </div>
                                    <input type="text"
                                            name="object_name"
                                            className={"form-control"}
                                            value={props.settings.object_name}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["main_obs"]}</span>
                                    </div>
                                    <input type="text"
                                            name="main_observer"
                                            className={"form-control"}
                                            value={props.settings.main_observer}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["seeing"]}</span>
                                    </div>
                                    <input type="number"
                                            name="seeing_meteo"
                                            className={"form-control"}
                                            value={props.settings.seeing_meteo}
                                            onChange={props.handleSettingChange}
                                            min="1"
                                            max="5"
                                    />
                                </div>
                            </TabPanel>
                            <TabPanel className="settingsEquipmentContent">
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["dir_path"]}</span>
                                    </div>
                                    <input type="text"
                                            name="directory_path"
                                            className={"form-control"}
                                            value={props.settings.directory_path}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["opt_name"]}</span>
                                    </div>
                                    <input type="text"
                                            name="optic_name"
                                            className={"form-control"}
                                            value={props.settings.optic_name}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["guide_opt_name"]}</span>
                                    </div>
                                    <input type="text"
                                            name="optic_guide_name"
                                            className={"form-control"}
                                            value={props.settings.optic_guide_name}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["ccd_name"]}</span>
                                    </div>
                                    <input type="text"
                                            name="ccd_name"
                                            className={"form-control"}
                                            value={props.settings.ccd_name}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["flt_list"]}</span>
                                    </div>
                                    <input type="text"
                                            name="filter_list"
                                            className={"form-control"}
                                            value={props.settings.filter_list}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                                <div className="input-group mb-2">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text">{param["flt_comments"]}</span>
                                    </div>
                                    <input type="text"
                                            name="filter_comments"
                                            className={"form-control"}
                                            value={props.settings.filter_comments}
                                            onChange={props.handleSettingChange}
                                    />
                                </div>
                            </TabPanel>
                        </Tabs>
                    </TabPanel>
                </Tabs>
            </div>
        </Popup>
    );
}

export default paramPopup;
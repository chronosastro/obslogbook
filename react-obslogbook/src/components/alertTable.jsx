import React, { Component } from 'react'
import './css/alertTable.css';

class alertTable extends Component {
   constructor(props) {
      super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
      this.state = { //state is by default an object
         alerts: []
      }
      props.alert(this.setAlert);
   }

   setAlert = (list) =>{
      this.setState({alerts:list});
   }

   renderTableData() {
    return this.state.alerts.map((alert, index) => {
       const { id, name, ra, dec } = alert //destructuring
       return (
          <tr key={id}>
             <td className={"ant-table-cell"}>{id}</td>
             <td className={"ant-table-cell"}>{name}</td>
             <td className={"ant-table-cell"}>{ra}</td>
             <td className={"ant-table-cell"}>{dec}</td>
          </tr>
       )
    })
}

renderTableHeader() {
   var title=window["translations"]().title;
    let header = [title._id.toString(), title.object_name.toString(), title.ra_target_j2000.toString(), title.dec_target_j2000.toString()];
    return header.map((key, index) => {
       return <th key={index}>{key}</th>
    })
 }

render() {
   var title=window["translations"]().title;
   if (this.state.alerts === undefined || this.state.alerts[0] === undefined){
      return(
         <div>
            <span className="text-danger mr-15">Error:  check your inputs</span>
         </div>
      )
   }else{
      return (
         <div>
            <h1 id='title'></h1>
            <table id='alerts'>
               <tbody>
                  <tr>{this.renderTableHeader()}</tr>
                  {this.renderTableData()}
               </tbody>
            </table>
         </div>
      )
   }
 }
}

export default alertTable
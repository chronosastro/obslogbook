import React, {useEffect, useRef, useState} from 'react';
import './components/css/App.css';
import NavBar from './components/navBar';
import InputList from "./components/inputList";
import SessionTracker from "./components/sessionTracker";
import {Row} from 'react-bootstrap';
import Search from "./components/search";
import Alert from "./components/alert"
import WarningsPopup from "./components/popups/warningsPopup";
import ParamPopup from "./components/popups/paramPopup";
import{Image} from "react-bootstrap"; 
import 'react-tabs/style/react-tabs.css';
import axios from "axios";

//Language files
import list_button_en from "./components/dataJson/en/list_button_en.json";
import list_button_fr from "./components/dataJson/fr/list_button_fr.json";
import list_button_es from "./components/dataJson/es/list_button_es.json";
import list_description_en from "./components/dataJson/en/list_description_en.json";
import list_description_fr from "./components/dataJson/fr/list_description_fr.json";
import list_description_es from "./components/dataJson/es/list_description_es.json";
import param_en from "./components/dataJson/en/param_en.json";
import param_fr from "./components/dataJson/fr/param_fr.json";
import param_es from "./components/dataJson/es/param_es.json";
import title_en from "./components/dataJson/en/title_en.json";
import title_fr from "./components/dataJson/fr/title_fr.json";
import title_es from "./components/dataJson/es/title_es.json";
import option_en from './components/dataJson/en/list_option_en.json';
import option_fr from './components/dataJson/fr/list_option_fr.json';
import option_es from './components/dataJson/es/list_option_es.json';
import warnings_messages_en from "./components/dataJson/en/warnings_messages_en.json";
import warnings_messages_fr from "./components/dataJson/fr/warnings_messages_fr.json";
import warnings_messages_es from "./components/dataJson/es/warnings_messages_es.json";
import target_and_refstar_info_fr from './components/dataJson/fr/target_and_refstar_info_fr.json';
import target_and_refstar_info_en from './components/dataJson/en/target_and_refstar_info_en.json';
import target_and_refstar_info_es from './components/dataJson/es/target_and_refstar_info_es.json';
import charts_fr from './components/dataJson/fr/charts_fr.json';
import charts_en from './components/dataJson/en/charts_en.json';
import charts_es from './components/dataJson/es/charts_es.json';
import import_fr from './components/dataJson/fr/import_messages_fr.json';
import import_en from './components/dataJson/en/import_messages_en.json';
import import_es from './components/dataJson/es/import_messages_es.json';
import config from './config.json'
import arrowUp from './resources/arrowUp.png'



function App() {

    //The app will not render until this is set to false, to give time to fetch settings from the api
    const [isBusy, setBusy] = useState(true);
    const [popupVisible, setPopupVisible] = useState(false);
    const [warningsNewSessionVisible, setWarningsNewSessionVisible] = useState(false);
    const [warningsNewObservationVisible, setWarningsNewObservationVisible] = useState(false);
    const navBar=useRef(null);
    window["navBar"]=navBar;
    const urlAPI=config.urlAPI;
    const refInputList=useRef(null);

    //these are the settings we can retrieve from the config file
    const [settings, setSettings] = useState({
        mongoURI: "",
        urlAlert: "",
        directory_path:"",
        object_name: "",
        main_observer: "",
        seeing_meteo: "",
        acquisition_name: "",
        acquisition_type: "",
        acquisition_result: "",
        optic_name: "",
        optic_guide_name: "",
        ccd_name: "",
        ccd_horizontal_binning: "",
        filter_list: "",
        filter_comments: "",
        db_name:"",
        collection_name:""
    });

    

    const [searchVisible, setSearchVisible] = useState(false);
    const [alertVisible, setAlertVisible] = useState(false);

    //We initialize sessionId with a new session, in case of there is not saved.
    //If there is one sessionId saved in LocalStorage, this value here will be replaced.
    const [sessionId, setSessionId] = useState(() => {
        let currentDate = new Date();
        return currentDate.getFullYear()
            + ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1)
            + (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate() + "_"
            + (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours()
            + (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes()
            + (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();
    });

    const [dateCreationObservation, setDateCreationObservation] = useState(() => {
        let currentDate = new Date();
        return currentDate.getFullYear()
            + ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1)
            + (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate() + "_"
            + (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours()
            + (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes()
            + (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();
    });
    /**
     * This react hook allows to simulate ComponentDidMount on a functional component.
     * It does not trigger on Update because there is a [] as second argument.
     */
    useEffect(() => {
        //We fetch the settings in the config file from the API
        axios.get(urlAPI+"/settings/get")
            .then(async function (response) {
                let data = response.data;

                //The config file is a json, so we can already set it without reformatting data.
                await setSettings(data);

                //if there is one sessionId saved, we restore it, if not, we store the new one to the localStorage.
                if (localStorage.getItem('ObsLogBookSessionId')) {
                    setSessionId(localStorage.getItem('ObsLogBookSessionId'));
                } else {
                    localStorage.setItem('ObsLogBookSessionId', sessionId);
                }

                //We indicate to the app we finished to retrieve all the settings.
                setBusy(false);
            })
            .catch(function (error) {
                //Catch will be triggered if the API don't find a config file, so the app launches without settings.
                console.log(error);
                setBusy(false);
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    /**
     * This method is nearly identical to the sessionId initialization and allows to generate a new sessionId
     * Or more precisely a date to format yyyymmdd_hhmmss, which is our sessionId.
     */
    let getDate = () => {
        let currentDate = new Date();
        let dateTime = currentDate.getFullYear()
            + ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1)
            + (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate() + "_"
            + (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours()
            + (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes()
            + (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();
        return dateTime;
    };

    /**
     * This method is passed to the navBar in props and is executed when the user click on the New Session button.
     */
    let handleNewSession = async () => {
        closePopup();
        setSearchVisible(false);
        setAlertVisible(false);
        navBar.current.setStatusExterne("searchButton");
        
        let dateTime = getDate();
        setSessionId(dateTime);
        setDateCreationObservation(dateTime);
        //We store the sessionId in the localStorage of the browser, it doesn't need to be in the config file.
        localStorage.setItem('ObsLogBookSessionId', dateTime);
    };

    /**
     * This method is nearly the same as handleNewSession, but does not generate a new sessionId
     */
    let handleNewObservation = () => {
        refInputList.current.setState({start_time:undefined})
        closePopup();
        setSearchVisible(false);
        setAlertVisible(false);
        navBar.current.setStatusExterne("searchButton");
        let dateTime = getDate();
        setDateCreationObservation(dateTime);
        
    };

    /**
     * This is used to show the settings popup, as the popup is in a controlled mode
     */
    let handleSettingsPopup = () => {
        setPopupVisible(true);
    };

    /**
     * When a value is changed in the settings Popup
     */
    let handleSettingChange = ({target}) => {
        setSettings({
            ...settings,
            [target.name]: target.value
        })
    }

    /**
     * This method is executed when we leave the settings popup to save the settings into the config file
     */
    let saveSettings = async () => {
        closePopup();

        let settingsJson = settings;
        //Save settings
        axios.post(urlAPI+'/settings/create', {
            settingsJson
        })
            .catch(function (error) {
                console.log(error);
            });
    }

    /**
     * This method is executed via the Search button in the navBar
     * It allows to hide the interface of observations to show the search interface
     */
    let handleSearchVisible = () => {
        if (searchVisible === false) {
            setSearchVisible(true)
            setAlertVisible(false)
            document.getElementById("inputList").style.display = "none";
            document.getElementById("sessionTracker").style.display = "none";
            document.getElementById("alert").style.display = "none";
            document.getElementById("search").style.display = "inline";
        } else {
            setSearchVisible(false)
            setAlertVisible(false)
            document.getElementById("inputList").style.display = "inline";
            document.getElementById("sessionTracker").style.display = "inline";
            document.getElementById("alert").style.display = "none";
            document.getElementById("search").style.display = "none";
        }
    }
    
    let handleAlertVisible = () => {
        if (alertVisible === false) {
            setSearchVisible(false)
            setAlertVisible(true)
            document.getElementById("inputList").style.display = "none";
            document.getElementById("sessionTracker").style.display = "none";
            document.getElementById("alert").style.display = "inline";
            document.getElementById("search").style.display = "none";
        } else {
            setSearchVisible(false)
            setAlertVisible(false)
            document.getElementById("inputList").style.display = "inline";
            document.getElementById("sessionTracker").style.display = "inline";
            document.getElementById("alert").style.display = "none";
            document.getElementById("search").style.display = "none";
        }
    }    

    

    /**
     * This is used to show the warning popup of new session, as the popup is in a controlled mode
     */
    let handleWarningsNewSessionPopup = () => {
        setWarningsNewSessionVisible(true);
    };

    /**
     * This is used to show the warning popup of the new observation, as the popup is in a controlled mode
     */
    let handleWarningsNewObservationPopup = () => {
        setWarningsNewObservationVisible(true);
    };
    
    /**
     * This is used to close all popups from the site
     */
    let closePopup = () => {
        if ( popupVisible ) {
            setPopupVisible(false);
        }
        if ( warningsNewObservationVisible ) {
            setWarningsNewObservationVisible(false);
        }
        if ( warningsNewSessionVisible ) {
            setWarningsNewSessionVisible(false);
        }
    }

    let handleClick = () =>{
        document.querySelector('html').scrollTop = 0; 
    }

    window.addEventListener("scroll", function(){
        if(document.getElementById("up")!==null){
            if(document.querySelector('html').scrollTop !== 0){
                document.getElementById("up").hidden = false; 
            }
            else if(document.querySelector('html').scrollTop === 0){
                document.getElementById("up").hidden = true; 
            }
        }
    },true)

    /** Current language of the app*/
    const [language, setLanguage] = useState("en");

    /** On language changing we use the function changeLang to change language files */
    useEffect(()=>{
       changeLang(language);
    },[language])
    
    /** Language files */
    const [description,setDescription]=useState(list_description_en);
    const [title,setTitle]=useState(title_en);
    const [button,setButton]=useState(list_button_en);
    const [option,setOption]=useState(option_en);
    const [warnings_message,setWarningMessage]=useState(warnings_messages_en);
    const [param,setParam]=useState(param_en);
    const [info, setInfo]=useState(target_and_refstar_info_en);
    const [charts, setCharts]=useState(charts_en);
    const [import_message,setImportMessage]=useState(import_en);

    /** Function for changing language files with @param lang a String for language*/
    let changeLang = (lang) => {
        if ( lang ==="fr") {
            setDescription(list_description_fr);
            setTitle(title_fr);
            setButton(list_button_fr);
            setWarningMessage(warnings_messages_fr);
            setParam(param_fr);
            setOption(option_fr);
            setInfo(target_and_refstar_info_fr);
            setCharts(charts_fr);
            setImportMessage(import_fr);
        }
        if ( lang === "en" ) {
            setDescription(list_description_en);
            setTitle(title_en);
            setButton(list_button_en);
            setWarningMessage(warnings_messages_en);
            setParam(param_en);
            setOption(option_en);
            setInfo(target_and_refstar_info_en);
            setCharts(charts_en);
            setImportMessage(import_en);
        }
        if ( lang === "es" ) {
            setDescription(list_description_es);
            setTitle(title_es);
            setButton(list_button_es);
            setWarningMessage(warnings_messages_es);
            setParam(param_es);
            setOption(option_es);
            setInfo(target_and_refstar_info_es);
            setCharts(charts_es);
            setImportMessage(import_es);
        }
    }
    /** Function to add at props of all objects we want to translate*/
    let getTranslations=()=>{
        return {title,description,button,warnings_message,param,option,info,charts,import_message};
    }
    window['translations']=getTranslations;

    //We wait isBusy is set to false, in the meantime we render a "fetching settings" message
    if (!isBusy) {
        return (
            <React.Fragment>
                {/* We give to all the components all the values they need, sessionId, dateCreationObservations
                 settings, and of course handle functions */}
                <NavBar
                    handleSettingsPopup={handleSettingsPopup}
                    handleSearchVisible={handleSearchVisible}
                    handleWarningsNewSessionPopup={handleWarningsNewSessionPopup}
                    handleWarningsNewObservationPopup={handleWarningsNewObservationPopup}
                    handleAlertVisible={handleAlertVisible}
                    language={language}
                    setLang={setLanguage}
                    ref={navBar}
                />
                <main className="container w-100 mw-100 marginBar" key={dateCreationObservation}>
                    <Row className="d-flex justify-content-center mt-5"> 
                        <div id="sessionTracker" className={"mr-5 mt-4"}>
                            <SessionTracker
                                sessionId={sessionId}
                                urlAPI={urlAPI}
                            />
                        </div>
                        <div id="inputList" className={"w-75"}>
                            <InputList
                                ref={refInputList}
                                sessionId={sessionId}
                                dateCreationObservation={dateCreationObservation}
                                settings={settings}
                                lang={language}
                                urlAPI={urlAPI}
                            />
                        </div>
                        <div id="search" style={{display: "none"}} className={"w-100"}>
                            <Search 
                                inputList={refInputList}
                                urlAPI={urlAPI}
                            />
                        </div>
                        <div id="alert" style={{display: "none"}} className={"w-100"}>
                            <Alert 
                                inputList={refInputList}
                                urlAPI={urlAPI}
                                urlAlert={settings.urlAlert}
                            />
                        </div>
                    </Row>
                </main>

                {/* This popup is the settings popup, it's invisible by default */}
                <ParamPopup
                    visible={popupVisible}
                    closeParamPopup={saveSettings}    
                    handleSettingChange={handleSettingChange}
                    settings={settings}
                    setLang={setLanguage}
                    lang={language}
                /> 

                {/* This popup is the warning popup for new sessions, it's invisible by default */}
                <WarningsPopup
                    visible={warningsNewSessionVisible}
                    closeWarningsPopup={closePopup}
                    handleAction={handleNewSession}
                    type="session"
                />

                {/* This popup is the warning popup for new observations, it's invisible by default */}
                <WarningsPopup
                    visible={warningsNewObservationVisible}
                    closeWarningsPopup={closePopup}
                    handleAction={handleNewObservation}
                    type="observation1"
                />
               <Image id='up' onClick={handleClick} src={arrowUp} roundedCircle hidden></Image>
            </React.Fragment>
        );
    }
    return (
        <div>
            <p>Fetching settings...</p>
            <p>Please wait...</p>
        </div>
    );
}

export default App;

from flask import Flask,jsonify,request,Response,json
import requests
from flask_cors import CORS
from flask_pymongo import PyMongo
import PyIndi
import sys
import logging
import time
from datetime import datetime
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, FK5, AltAz, EarthLocation
from astroquery.simbad import Simbad


class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
        self.logger = logging.getLogger('IndiClient')
        self.logger.info('creating an instance of IndiClient')
    def newDevice(self, d):
        self.logger.info("new device " + d.getDeviceName())
    def newProperty(self, p):
        self.logger.info("new property "+ p.getName() + " for device "+ p.getDeviceName())
    def removeProperty(self, p):
        self.logger.info("remove property "+ p.getName() + " for device "+ p.getDeviceName())
    def newBLOB(self, bp):
        self.logger.info("new BLOB "+ bp.name)
    def newSwitch(self, svp):
        self.logger.info ("new Switch "+ svp.name + " for device "+ svp.device)
    def newNumber(self, nvp):
        self.logger.info("new Number "+ nvp.name + " for device "+ nvp.device)
    def newText(self, tvp):
        self.logger.info("new Text "+ tvp.name + " for device "+ tvp.device)
    def newLight(self, lvp):
        self.logger.info("new Light "+ lvp.name + " for device "+ lvp.device)
    def newMessage(self, d, m):
        self.logger.info("new Message "+ d.messageQueue(m))
    def serverConnected(self):
        self.logger.info("Server connected ("+self.getHost()+":"+str(self.getPort())+")")
    def serverDisconnected(self, code):
        self.logger.info("Server disconnected (exit code = "+str(code)+","+str(self.getHost())+":"+str(self.getPort())+")")


app = Flask(__name__, static_folder='../build', static_url_path='/')
apiKey="ee07e2bf337034f905cde0bdedae3db8"
CORS(app)
#following function will configure the mongo URI, if the config file exist and contain the URI it will be stored into app.config["MONGO_URI"]
#If it's note the default URI is mongodb://localhost:27017/test
def handleMongoURI():
	try:
		with open('config.json') as json_file:
			data = json.load(json_file)
			mongoURI=data["mongoURI"]
			if(mongoURI is not None and mongoURI!=""):
				app.config["MONGO_URI"] = mongoURI
			else:
				app.config["MONGO_URI"] = "mongodb://localhost:27017/test"
	except:
			app.config["MONGO_URI"] = "mongodb://localhost:27017/test"

handleMongoURI()
mongo = PyMongo(app)

@app.route("/")
def index():
	return app.send_static_file('index.html')

"""
The following route get all the observation's environment data with indi server such as latitude, longitude, elevation and the current date.

If an indi server is connected it should return a status code 200 and the informations.
If an indi server is NOT connected it should return a status code 421 and a exemple of the command to start an indi server
"""
@app.route("/get/environment",methods=['GET'])
def getEnvironment():
	ret={}
	indiclient=IndiClient()
	indiclient.setServer("localhost",7624)
	# Connect to server
	if (not(indiclient.connectServer())):
		return Response("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run: indiserver indi_simulator_telescope indi_simulator_ccd", status=421,mimetype="application/json")
		sys.exit(1)
	time.sleep(1)

	device_telescope=None

	dl=indiclient.getDevices()
	#We seek to get the telescope in order to get the localisation later
	for device in dl:
		#the device is the telescope (interface = 5)
		if(str(device.getDriverInterface())=="5"):
			device_telescope=indiclient.getDevice(device.getDeviceName())
			while not(device_telescope):
				time.sleep(0.5)
				device_telescope=indiclient.getDevice(device.getDeviceName())

	#Get all the information with telescope if it was found before
	if(device_telescope is not None):
		# wait CONNECTION property be defined for telescope
		telescope_connect=device_telescope.getSwitch("CONNECTION")
		while not(telescope_connect):
			time.sleep(0.5)
			telescope_connect=device_telescope.getSwitch("CONNECTION")

		# if the telescope device is not connected, we do connect it
		if not(device_telescope.isConnected()):
			# Property vectors are mapped to iterable Python objects
			# Hence we can access each element of the vector using Python indexing
			# each element of the "CONNECTION" vector is a ISwitch
			telescope_connect[0].s=PyIndi.ISS_ON  # the "CONNECT" switch
			telescope_connect[1].s=PyIndi.ISS_OFF # the "DISCONNECT" switch
			indiclient.sendNewSwitch(telescope_connect) # send this new value to the device

		#Wait until the telescope is connect
		while not(device_telescope.isConnected()):
			time.sleep(0.5)

		#Get telescope localisation
		localisation = None
		localisation = getLocalisation(device_telescope)
		if(localisation is not None):
			lat=str(localisation[0].value)
			long=str(localisation[1].value)
			elev=str(localisation[2].value)
			ret['latitude']=lat
			ret['longitude']=long
			ret['elevation']=elev
			r = requests.get("http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+long+"&appid="+apiKey)
			data=r.json()
			#Get sky status
			ret['sky']=data['weather'][0]['main']
			#Get humidity status
			ret['humidity']=data['main']['humidity']
	indiclient.disconnectServer()
	response=app.response_class(response=json.dumps(ret),status=200, mimetype='application/json')
	return response

"""
The following route get all the observation's equipment data with indi server such as ccd temperature, ccd binding, telescope's name...

If an indi server is connected it should return a status code 200 and the informations.
If an indi server is NOT connected it should return a status code 421 and a exemple of the command to start an indi server
"""
@app.route("/get/equipment", methods=['GET'])
def getEquipment():
	ret={}
	indiclient=IndiClient()
	indiclient.setServer("localhost",7624)
	# Connect to server
	if (not(indiclient.connectServer())):
		return Response("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run: indiserver indi_simulator_telescope indi_simulator_ccd", status=421,mimetype="application/json")
		sys.exit(1)
	time.sleep(1)

	#we get all the connected devices in order to get the telescope and the ccd later
	dl=indiclient.getDevices()
	#We first assign None to device_telescope and device_ccd so we can check later if a ccd or a telescope was found
	device_telescope=None
	device_ccd=None
	for device in dl:
		#if the device is the telescope (interface = 5)
		if(str(device.getDriverInterface())=="5"):
			device_telescope=indiclient.getDevice(device.getDeviceName())
			ret["mount"]=str(device.getDeviceName())
		#if the device is the ccd (interface = 22)
		elif(str(device.getDriverInterface())=="22"):
			device_ccd=indiclient.getDevice(device.getDeviceName())
			ret["mainCCDName"]=str(device.getDeviceName())

	#We get telescope data if a telescope was found in connected devices
	if(device_telescope is not None):
		# wait CONNECTION property be defined for telescope
		telescope_connect=device_telescope.getSwitch("CONNECTION")
		while not(telescope_connect):
			time.sleep(0.5)
			telescope_connect=device_telescope.getSwitch("CONNECTION")

		# if the telescope device is not connected, we do connect it
		if not(device_telescope.isConnected()):
			# Property vectors are mapped to iterable Python objects
			# Hence we can access each element of the vector using Python indexing
			# each element of the "CONNECTION" vector is a ISwitch
			telescope_connect[0].s=PyIndi.ISS_ON  # the "CONNECT" switch
			telescope_connect[1].s=PyIndi.ISS_OFF # the "DISCONNECT" switch
			indiclient.sendNewSwitch(telescope_connect) # send this new value to the device

		#Wait until the telescope is connect
		while not(device_telescope.isConnected()):
			time.sleep(0.5)

		#get telescope informations:
		telescopeInformation = None
		telescopeInformation = getTelescopeInfos(device_telescope)
		if(telescopeInformation is not None):
			for info in telescopeInformation:
				ret[info.name]=str(info.value)

	#We get ccd data if a ccd was found in connected devices
	if(device_ccd is not None):
		ccd_connect=device_ccd.getSwitch("CONNECTION")
		while not(ccd_connect):
		    time.sleep(0.5)
		    ccd_connect=device_ccd.getSwitch("CONNECTION")
		if not(device_ccd.isConnected()):
		    ccd_connect[0].s=PyIndi.ISS_ON  # the "CONNECT" switch
		    ccd_connect[1].s=PyIndi.ISS_OFF # the "DISCONNECT" switch
		    indiclient.sendNewSwitch(ccd_connect)
		#Wait until the ccd is connect
		while not(device_ccd.isConnected()):
			time.sleep(0.5)

		ccdInformation=None
		ccdInformation = getCCDInfos(device_ccd)
		if(ccdInformation is not None):
			for info in ccdInformation:
				ret[info.name]=str(info.value)

		ccdTemperature=None
		ccdTemperature = device_ccd.getNumber("CCD_TEMPERATURE")
		if(ccdTemperature is not None):
			ret["temperature"]=str(ccdTemperature[0].value)


		ccdFrame=None
		ccdFrame = device_ccd.getNumber("CCD_FRAME")
		if(ccdFrame is not None):
			for info in ccdFrame:
				ret[info.name]=str(info.value)


		ccdFrame = None
		ccdFrame = device_ccd.getNumber("CCD_BINNING")
		if(ccdFrame is not None):
			for info in ccdFrame:
				ret[info.name]=str(info.value)

		ccdExposure = None
		ccdExposure = device_ccd.getNumber("CCD_EXPOSURE")
		if(ccdExposure is not None):
			ret["exposure"]=str(ccdExposure[0].value)

	indiclient.disconnectServer()
	response=app.response_class(response=json.dumps(ret),status=200, mimetype='application/json')
	return response


"""
The following route get all the observation's acquisition data with indi server such as JNOW ra dec, J2000 ra dec and horizontal coordinates.

If an indi server is connected it should return a status code 200 and the informations.
If an indi server is NOT connected it should return a status code 421 and a exemple of the command to start an indi server
"""
@app.route("/get/acquisition", methods=['GET'])
def getAcquisition():
	ret={}
	indiclient=IndiClient()
	indiclient.setServer("localhost",7624)
	# Connect to server
	if (not(indiclient.connectServer())):
		return Response("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort())+" - Try to run: indiserver indi_simulator_telescope indi_simulator_ccd", status=421,mimetype="application/json")
		sys.exit(1)
	time.sleep(1)

	device_telescope=None
	dl=indiclient.getDevices()
	for device in dl:
		#if the device is the telescope (interface = 5)
		if(str(device.getDriverInterface())=="5"):
			device_telescope=indiclient.getDevice(device.getDeviceName())
		#if the device is the ccd (interface = 22)
		elif(str(device.getDriverInterface())=="22"):
			device_ccd=indiclient.getDevice(device.getDeviceName())


	#Get all the information with telescope if it was found before
	if(device_telescope is not None):
		# wait CONNECTION property be defined for telescope
		telescope_connect=device_telescope.getSwitch("CONNECTION")
		while not(telescope_connect):
			time.sleep(0.5)
			telescope_connect=device_telescope.getSwitch("CONNECTION")

		# if the telescope device is not connected, we do connect it
		if not(device_telescope.isConnected()):
			# Property vectors are mapped to iterable Python objects
			# Hence we can access each element of the vector using Python indexing
			# each element of the "CONNECTION" vector is a ISwitch
			telescope_connect[0].s=PyIndi.ISS_ON  # the "CONNECT" switch
			telescope_connect[1].s=PyIndi.ISS_OFF # the "DISCONNECT" switch
			indiclient.sendNewSwitch(telescope_connect) # send this new value to the device

		#Wait until the telescope is connect
		while not(device_telescope.isConnected()):
			time.sleep(0.5)

		#get JNOW ra dec
		telescope_jnow_radec=None
		telescope_jnow_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
		if(telescope_jnow_radec is not None):
			ret["jnow_ra"]=str(telescope_jnow_radec[0].value)
			ret["jnow_dec"]=str(telescope_jnow_radec[1].value)

		#convert into J2000 ra dec
		if(telescope_jnow_radec is not None):
			c_icrs = SkyCoord(ra=telescope_jnow_radec[0].value*u.degree, dec=telescope_jnow_radec[1].value*u.degree, frame='icrs')
			c_j2000= c_icrs.transform_to('fk5')
			ret["j2000_ra"]=str(c_j2000.ra.degree)
			ret["j2000_dec"]=str(c_j2000.dec.degree)

			date=datetime.now()
			timeAstropy=Time(date.strftime("%Y-%m-%d %H:%M:%S"))
			localisation = None
			localisation = getLocalisation(device_telescope)
			if(localisation is not None):
				latitude=localisation[0].value*u.deg
				longitude=localisation[1].value*u.deg
				elevation=localisation[2].value*u.m
			localisation=EarthLocation(lat=latitude,lon=longitude,height=elevation)
			c_altaz= c_icrs.transform_to(AltAz(obstime=timeAstropy,location=localisation))
			ret["alt"]=str(c_altaz.alt)
			ret["az"]=str(c_altaz.az)


	indiclient.disconnectServer()
	response=app.response_class(response=json.dumps(ret),status=200, mimetype='application/json')
	return response



"""
The following route save an observation's form into the data base.
It should return a 201 status code with the document created
"""
@app.route("/post/form",methods=['POST'])
def postForm():
	mongoInsertRequest = {}
	#We get the form data
	formInfo= request.json["form"]
	#We have to create our own dictionary  because request.form return an ImmutableMultiDict which can't be save directly into the database
	for info in formInfo:
		mongoInsertRequest[info] = formInfo[info]
	#We store our dictionary  into the database
	mongo.db.test.insert_one(mongoInsertRequest)
	return Response(str(mongoInsertRequest.items()),status=201, mimetype='application/json')

"""
The following route save many observation's forms into the data base.
It should return a 201 status code with succes message
"""
@app.route("/post/multiforms",methods=['POST'])
def postMultiforms():
    mongoInsertRequest = []
    #We get the form data
    formsInfo= request.json["forms"]
    #We have to create our own dictionary  because request.form return an ImmutableMultiDict which can't be save directly into the database
    for i in range(len(formsInfo)):
        mongoInsertRequest.append(formsInfo[i])
    #We store our dictionary  into the database
    mongo.db.obsbooktable.insert_many(mongoInsertRequest)
    return Response(str("successful insertions"),status=201, mimetype='application/json')
	
"""
The following route save an observation's form into the data base.
It should return a 201 status code with the document created
"""
@app.route("/post/updateForm",methods=['POST'])
def updateForm():
	try:
		with open('config.json') as json_file:
			data = json.load(json_file)
			mongoInsertData = {}
			#We get the form data
			formInfo= request.json["form"]
			#We have to create our own dictionary  because request.form return an ImmutableMultiDict which can't be save directly into the database
			for info in formInfo:
				mongoInsertData[info] = formInfo[info]
			mongoInsertRequest={"$set":mongoInsertData}
			#We store our dictionary  into the database
			mongo.db.test.update_many({"observationId":mongoInsertData["observationId"]},mongoInsertRequest)
			return Response(str(mongoInsertRequest.items()),status=201, mimetype='application/json')
	except FileNotFoundError:
		return Response("No settings file found", status=400,mimetype="application/json")




def getLocalisation(telescope):
	return telescope.getNumber("GEOGRAPHIC_COORD")

def getTelescopeInfos(telescope):
	return telescope.getNumber("TELESCOPE_INFO")

def getCCDInfos(ccd):
	return ccd.getNumber("CCD_INFO")

"""
The following route return all observations.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obs/",methods=['GET'])
def getObs():
        observations=mongo.db.test.find({}).sort([("date",-1)])
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)

"""
The following route return all observations with a specified session_id.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsSessionId/<session_id>",methods=['GET'])
def getObsSessionId(session_id):
        observations=mongo.db.test.find({"sessionId": session_id})
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)


"""
The following route return all observations with a specified session_id and observationType.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsSessionId/<session_id>/<type>",methods=['GET'])
def getObsSessionIdAndType(session_id,type):
        observations=mongo.db.test.find({"sessionId": session_id,"observationType":type})
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)

"""
The following route return all observations with a specified object name.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsObjectName/<objectName>",methods=['GET'])
def getObsObjectName(objectName):
        observations=mongo.db.test.find({"objectName": objectName})
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)

"""
The following route return all observations with a specified object name and an observation's type.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsObjectName/<objectName>/<type>",methods=['GET'])
def getObsObjectNameAndType(objectName,type):
		observations=mongo.db.test.find({"objectName": objectName,"observationType":type})
		#Convert the cursor into a list we can return
		obsList=[]
		for obs in observations:
			obs['_id'] = str(obs['_id'])
			obsList.append(obs)
		return jsonify(obsList)


"""
The following route return all observations with a specified date.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsDate/<date>",methods=['GET'])
def getObsDate(date):
        observations=mongo.db.test.find({"date": date})
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)


"""
The following route return all observations with a specified date and observation type.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsDate/<date>/<type>",methods=['GET'])
def getObsDateAndType(date):
        observations=mongo.db.test.find({"date": date,"observationType":type})
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)

"""
The following route return all observations with a specified observation type.
It should return a status code 200 and the list of observations
"""
@app.route("/get/obsType/<type>",methods=['GET'])
def getObsType(type):
        observations=mongo.db.test.find({"observationType": type})
        #Convert the cursor into a list we can return
        obsList=[]
        for obs in observations:
                obs['_id'] = str(obs['_id'])
                obsList.append(obs)
        return jsonify(obsList)


"""
The following route delete all observations from database.
"""
@app.route("/clear/form",methods=['GET'])
def clearForm():
        mongo.db.test.drop()
        return "Database clear"

"""
The following route create the settings file with json give by user inside the api folder.
"""
@app.route("/settings/create",methods=['POST'])
def handleSettingsFile():
	data=request.json["settingsJson"]
	# using with statement to create the file if it does not exist and write
	with open('config.json', 'w') as file:
		json.dump(data, file)
	handleMongoURI()
	global mongo
	mongo = PyMongo(app)
	return "Settings file created"

"""
The following route get the settings file in the api folder return settings value if the file exists and raise an error if not.
"""
@app.route("/settings/get",methods=['GET'])
def getSettingsFile():
		try:
			with open('config.json') as json_file:
				data = json.load(json_file)
		except FileNotFoundError:
			return Response("No settings file found", status=400,mimetype="application/json")
		response=app.response_class(response=json.dumps(data),status=200, mimetype='application/json')
		return response

"""
The followung route get the name of the observed object and returns the j2000 coordinates of this object if the object exists.
"""
@app.route("/getobjcoords/<name>",methods=['GET'])
def getObjCoords(name):
    #récupération d'une instance Simbad
    simbad = Simbad()

    result_table = None
    #requête
    simbad.add_votable_fields('sptype','rv_value','flux(V)','mt','otype','ra','dec','dim')
    result_table = simbad.query_object(name) # retourne un tuple
    print(result_table)

    if (result_table is not None):
        coord = []
        ra = str(result_table['RA'][0])
        dec = str(result_table['DEC'][0])
        stype = str(result_table['SP_TYPE'][0])
        rv_value = str(result_table['RV_VALUE'][0])
        flux_v = str(result_table['FLUX_V'][0])
        mt = str(result_table['MORPH_TYPE'][0])
        otype = str(result_table['OTYPE'][0])
        dim = str(result_table['GALDIM_ANGLE'][0])
        coord.append(ra)
        coord.append(dec)
        coord.append(stype)
        coord.append(rv_value)
        coord.append(flux_v)
        coord.append(mt)
        coord.append(otype)
        coord.append(dim)
        return jsonify(coord)
		
if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')

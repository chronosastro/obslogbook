FROM python:3
WORKDIR /app
COPY . /app
RUN pip install -r requirement.txt
EXPOSE 5050
ENV NOM env 
CMD ["python", "./react-obslogbook/api/apiBDD.py"]

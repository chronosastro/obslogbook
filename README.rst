==========
ObsLogBook
==========

ObsLogBook is an application for astronomer. It aim to simplify the data collection of observations using INDI server.

|npm| |python| |docker| |dependencies| |licence| 



Requirements: 
-----------
- OS: Ubuntu / Windows / MacOS
- Node: v15.8.0
- npm: 7.5.1
- MongoDB: v3.6.8(or online mongoDB)
- Astropy: https://www.astropy.org/
- (INDI: https://indilib.org/get-indi/download-ubuntu.html)


The following task has been tested in this environment, please install and use same versions to be sure everything works fine.

API Installation:
-------------
Be sure docker is installed !

Open a command line interface :

``docker pull matthieulel/obslogbook-api:latest``

``docker run -p 5050:5050 matthieulel/obslogbook-api``


Run Development version:
-----------------------
Download the project folder :
``git clone --branch react https://gitlab.com/chronosastro/obslogbook.git``

Go in the project folder then:
``npm install``
Launch the application :
``npm start``

⚠️ **Don't forget to go in settings and set the mongoDB database informations!** ⚠️


Run Production version:
-----------------------
Build the react application: npm run build --> The application is generated in the build/ folder
It's possible there is an error "javascript heap out of memory" when building on a raspberry pi 3B+ (maybe others as well).
Webkit Sourcemaps has been disabled to solve this problem, but it may reappear as the project grow.

Download the project folder :
``git clone --branch react https://gitlab.com/chronosastro/obslogbook.git``

Go in the project folder then:
``npm install``

Create the build :
``npm run build`` (On Windows ``npm run winBuild``)

Install serve :
``npm install -g serve``

Then launch the build :
``serve -s build``

Your application is accessible with the API URL (ex: 192.168.1.100:5000 and localhost:5000)

⚠️ **Don't forget to go in settings and set the mongoDB database informations!** ⚠️




.. |npm| image:: http://img.shields.io/badge/npm-6.14-blue
    :alt: npm
    :scale: 100%
    :target: https://www.npmjs.com/

.. |python| image:: https://img.shields.io/badge/Python-3.7%203.8-green
    :alt: Python
    :scale: 100%
    :target: https://www.python.org/

.. |dependencies| image:: https://img.shields.io/badge/dependencies-astropy-orange
    :alt: Dependencies
    :scale: 100%
    :target: https://www.astropy.org/

.. |licence| image:: https://img.shields.io/badge/licence-GPL%203.0-orange
    :alt: Licence gpl-v3.0
    :scale: 100%
    :target: https://www.gnu.org/licenses/gpl-3.0.fr.html

.. |docker| image:: https://img.shields.io/docker/pulls/matthieulel/obslogbook-api?label=Docker%20Pull
    :alt: Docker Pull
    :scale: 100%
    :target: https://hub.docker.com/repository/docker/matthieulel/obslogbook-api 
